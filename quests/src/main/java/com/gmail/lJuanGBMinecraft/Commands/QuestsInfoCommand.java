package com.gmail.lJuanGBMinecraft.Commands;

import java.util.List;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Quest.NamespacedKey;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Quest.QuestsMap;

public class QuestsInfoCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
		
		if(!cmd.getName().equals("questsinfo")) return true;
		
		if(!sender.hasPermission("quests.api.command.admin.info")) {
			return true;
		}
	
		
		if(args.length == 0) {
			Set<NamespacedKey> keys = QuestsMap.getAllKeys();
			for(NamespacedKey key : keys) {
				
				if(QuestsMap.getQuest(key).isLegacy()) {
					
					sender.sendMessage(ChatColor.GRAY + key.toString());
				} else {
					
					sender.sendMessage(key.toString());
				}
			}
			
			
		} else {
			
			Quest quest;
			try {
				NamespacedKey key = new NamespacedKey(args[2]);
				quest = QuestsMap.getQuest(key);
				
			} catch (Exception ex) {
				sender.sendMessage(ex.getMessage());
				return true;
			}
			
			List<String> messages = new AcceptedQuest(quest).getFullDescription("en_US", false);
			
			for(String message : messages) {
				sender.sendMessage(message);
			}
		}

		
		return true;
	}
}
