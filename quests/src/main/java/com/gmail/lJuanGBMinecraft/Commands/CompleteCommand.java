package com.gmail.lJuanGBMinecraft.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Quest.NamespacedKey;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Quest.QuestsMap;
import com.gmail.lJuanGBMinecraft.Task.AcceptedTask;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class CompleteCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
		
		if(!cmd.getName().equals("complete")) return true;
		
		if(!sender.hasPermission("quests.api.command.admin.complete")) {
			return true;
		}
		
		if(args.length <= 1) {
			
			return false;
		}
		
		Player player = Bukkit.getPlayer(args[0]);
		if(player == null) return false;
		
		switch(args[1].toLowerCase()) {
		
		case "allquests": {
			
			completeQuests(player);
			
			return true;}
		
		case "quest": {
			
			if(args.length < 3) return false;
			
			Quest quest;
			try {
				NamespacedKey key = new NamespacedKey(args[2]);
				quest = QuestsMap.getQuest(key);
				
			} catch (Exception ex) {
				sender.sendMessage(ex.getMessage());
				return true;
			}
			
			PlayerQuestHelper.completeQuest(player, quest, false);
			
			return true;}
		
		case "currenttasks": {
			
			if(args.length < 3) return false;
			
			Quest quest;
			try {
				NamespacedKey key = new NamespacedKey(args[2]);
				quest = QuestsMap.getQuest(key);
				
			} catch (Exception ex) {
				sender.sendMessage(ex.getMessage());
				return true;
			}
			
			completeCurrentTasks(player, quest);
			
			return true;}
		}
		
		return false;
	}

	
	private void completeCurrentTasks(Player player, Quest quest) {

		AcceptedQuest aQuest = PlayerQuestHelper.getAcceptedQuest(player, quest);
		
		if(aQuest == null) return;
		
		for(AcceptedTask task : aQuest.getCompleteableTasks()) {
			task.setActionCompleted( task.getTask().getActionGoal() );
			aQuest.taskCompleted(player, task);
		}
	}


	private void completeQuests(Player player) {
		
		for(AcceptedQuest quest : PlayerQuestHelper.getAcceptedQuests(player)) {
			PlayerQuestHelper.completeQuest(player, quest.getQuest(), false);
		}
	}
}
