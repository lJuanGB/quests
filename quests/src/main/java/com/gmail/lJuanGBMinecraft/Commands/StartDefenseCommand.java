package com.gmail.lJuanGBMinecraft.Commands;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.LanguageAPI.MessagePosition;
import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Quest.NamespacedKey;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Quest.QuestsMap;
import com.gmail.lJuanGBMinecraft.Task.Task;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.Defend.DefendTask;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.Defend.DefenseInstance;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.Defend.DefenseMap;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;
import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

import net.md_5.bungee.api.ChatColor;

public class StartDefenseCommand implements CommandExecutor{
	
	public static final String path = "generic.task.defend.startCommand.";
	
	public static final String alreadyOne = "alreadyStarted";
	public static final String noDefense = "noDefense";
	public static final String commandInstruction = "commandInstruction";
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
		
		if(!cmd.getName().equals("startdefense")) return false;
		
		//Command for the player to use on himself
		if(args.length == 0 &&  sender instanceof Player) {
			
			if(!sender.hasPermission("quests.api.command.startdefense")) {
				return true;
			}
			
			Player player = (Player) sender;
			
			startDefense(player);
			return true;
		}
		
		
		//If the player cannot send the command to others
		if(!sender.hasPermission("quests.api.command.startdefense.others")){
			
			if(sender.hasPermission("quests.api.command.startdefense") && sender instanceof Player) {
				
				QuestsAPIPlugin.api.sendMessage(MessagePosition.CHAT, (Player) sender, path + commandInstruction);
				return true;
				
			} else {
				return true;
			}
		}

		
		//Get the target player
		Player target = null;
		
		if(args[0].equals("@p") && UtilMethods.hasLocation(sender)) {
			
			Player senderPlayer = null;
			if(sender instanceof Player) senderPlayer = (Player) sender;
			
			Location loc = UtilMethods.getLocation(sender);
			
			target = UtilMethods.getClosestEntity(loc, Player.class, senderPlayer);
			
		} else {
			
			target = Bukkit.getPlayer(args[0]);
		}
		
		if(target == null) {
			
			sender.sendMessage(ChatColor.RED + "Could not find player: " + args[0]);
			return true;
		}
		
		
		//If the length is higher than 1, a quest may be specified. If its valid, start that quest
		if(args.length > 1) {
			
			NamespacedKey key;
			try {
				key = new NamespacedKey(args[1]);
				
			} catch (IllegalArgumentException ex) {
				
				sender.sendMessage(ChatColor.RED + "The quest ID indicated is not valid");
				return true;
			}
			
			Quest quest = QuestsMap.getQuest(key);
			
			if(quest == null) {
				
				sender.sendMessage(ChatColor.RED + "The quest ID is valid, but no quest exists with that ID");
				return true;
			}
			
			startDefense(target, quest);
			return true;
		}
		
		
		//If it only specifies player, act as if that player had run /startdefense
		startDefense(target);
		return true;
	}
	
	
	public static boolean startDefense(Player player, Quest quest) {
		
		if(!PlayerQuestHelper.isQuestAccepted(player, quest)) {
			
			return false;
		}
		
		if(DefenseMap.contains(player)) {
			
			QuestsAPIPlugin.api.sendMessage(MessagePosition.CHAT, player, path + alreadyOne);
			return false;
		}
		
		for(Task task : quest.getAllTasks()) {
			
			if(task instanceof DefendTask) {
				
				DefendTask dTask = (DefendTask) task;
				
				if(!dTask.isInRange(player.getLocation())) continue;
				
				DefenseInstance defense = new DefenseInstance(player, dTask);
				defense.start();
				return true;
			}
		}
		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean startDefense(Player player) {
		
		List<AcceptedQuest> quests = PlayerQuestHelper.getAcceptedQuests(player);
		Map<Region, Quest> regionMap = PlayerQuestHelper.getRegions(quests, player.getLocation(), DefendTask.class);
		
		if(regionMap.isEmpty()) {
			
			QuestsAPIPlugin.api.sendMessage(MessagePosition.CHAT, player, path + noDefense);
			return true;
		}
		
		Quest randQuest = UtilMethods.getRandom(regionMap.values());
		
		return startDefense(player, randQuest);
	}
}
