package com.gmail.lJuanGBMinecraft.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.LanguageAPI.MessagePosition;
import com.gmail.lJuanGBMinecraft.QuestBook.OpenBookMap;
import com.gmail.lJuanGBMinecraft.QuestBook.OpenQuestBook;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class QuestsCommand implements CommandExecutor{
	
	public static final String path = "menus.questBook.";
	
	public static final String empty = "empty";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
		
		if(!cmd.getName().equals("quests")) return true;
		
		if(!sender.hasPermission("quests.api.command.quests")) {
			return true;
		}
		
		if(!(sender instanceof Player)) {
			
			sender.sendMessage("Only a player can use this command");
			return true;
		}
		
		Player player = (Player) sender;
		
		if(PlayerQuestHelper.getAcceptedQuests(player).isEmpty()) {
			
			QuestsAPIPlugin.api.sendMessage(MessagePosition.CHAT, player, path + empty);
			return true;
		}
		
		OpenQuestBook book = new OpenQuestBook(player);
		OpenBookMap.register(book);
		book.openTab(null);
		return true;
	}

}
