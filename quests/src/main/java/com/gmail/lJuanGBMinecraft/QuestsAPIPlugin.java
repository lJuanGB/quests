package com.gmail.lJuanGBMinecraft;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.lJuanGBMinecraft.Commands.CompleteCommand;
import com.gmail.lJuanGBMinecraft.Commands.QuestsCommand;
import com.gmail.lJuanGBMinecraft.Commands.QuestsInfoCommand;
import com.gmail.lJuanGBMinecraft.Commands.StartDefenseCommand;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageAPI;
import com.gmail.lJuanGBMinecraft.PlayerListeners.MoveListener;
import com.gmail.lJuanGBMinecraft.PlayerListeners.PlayerJoinListener;
import com.gmail.lJuanGBMinecraft.PlayerListeners.PlayerLeaveListener;
import com.gmail.lJuanGBMinecraft.QuestBook.Listeners.BookClickListener;
import com.gmail.lJuanGBMinecraft.QuestBook.Listeners.InventoryCloseListener;

public class QuestsAPIPlugin extends JavaPlugin{

	private static QuestsAPIPlugin instance;
	
	public static boolean useMySql = false;
	
	
	public static LanguageAPI api = null;
	
	
	@Override
	public void onEnable() {
		
		instance = this;
		api.loadLocales("en_US");
		
		
		getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
		getServer().getPluginManager().registerEvents(new PlayerLeaveListener(), this);
		
		getServer().getPluginManager().registerEvents(new BookClickListener(), this);
		getServer().getPluginManager().registerEvents(new InventoryCloseListener(), this);

		getServer().getPluginManager().registerEvents(new MoveListener(), this);

		
		getCommand("quests").setExecutor(new QuestsCommand());
		getCommand("startdefense").setExecutor(new StartDefenseCommand());
		getCommand("complete").setExecutor(new CompleteCommand());
		getCommand("questsinfo").setExecutor(new QuestsInfoCommand());

	}
	
	@Override
	public void onDisable() {
		
	}

	
	public static QuestsAPIPlugin getInstance() {
		return instance;
	}

}
