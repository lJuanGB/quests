package com.gmail.lJuanGBMinecraft.Quest;

import java.util.Locale;
import java.util.regex.Pattern;

import org.bukkit.plugin.Plugin;

import com.google.common.base.Preconditions;

/**
 * Copied from Bukkit, since 1.9 Bukkit does not contain this class yet
 */
public final class NamespacedKey {

    private static final Pattern VALID_NAMESPACE = Pattern.compile("[a-z0-9._-]+");
    private static final Pattern VALID_KEY = Pattern.compile("[a-z0-9/._-]+");
    
    public static final String keySeparator = ":";
    
    private final String namespace;
    private final String key;


    /**
     * Create a key in the plugin's namespace.
     * <p>
     * Namespaces may only contain lowercase alphanumeric characters, periods,
     * underscores, and hyphens.
     * <p>
     * Keys may only contain lowercase alphanumeric characters, periods,
     * underscores, hyphens, and forward slashes.
     *
     * @param plugin the plugin name to use for the namespace
     * @param key the key to create
     */
    public NamespacedKey(String plugin, String key) {
        Preconditions.checkArgument(plugin != null, "Plugin cannot be null");
        Preconditions.checkArgument(key != null, "Key cannot be null");

        this.namespace = plugin.toLowerCase().toLowerCase(Locale.ROOT);
        this.key = key.toLowerCase().toLowerCase(Locale.ROOT);

        // Check validity after normalization
        Preconditions.checkArgument(VALID_NAMESPACE.matcher(this.namespace).matches(), "Invalid namespace. Must be [a-z0-9._-]: %s", this.namespace);
        Preconditions.checkArgument(VALID_KEY.matcher(this.key).matches(), "Invalid key. Must be [a-z0-9/._-]: %s", this.key);

        String string = toString();
        Preconditions.checkArgument(string.length() < 256, "NamespacedKey must be less than 256 characters (%s)", string);
    }
    

    /**
     * Create a key in the plugin's namespace.
     * <p>
     * Namespaces may only contain lowercase alphanumeric characters, periods,
     * underscores, and hyphens.
     * <p>
     * Keys may only contain lowercase alphanumeric characters, periods,
     * underscores, hyphens, and forward slashes.
     *
     * @param plugin the plugin name to use for the namespace
     * @param key the key to create
     */
    public NamespacedKey(Plugin plugin, String key) {
    	this(plugin.getName(), key);
    }
    
    
    public NamespacedKey(String namespacedString) {
    	this(separateNamespacedString(namespacedString)[0], separateNamespacedString(namespacedString)[1]);
    }
    



	public String getNamespace() {
        return namespace;
    }

    public String getKey() {
        return key;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.namespace.hashCode();
        hash = 47 * hash + this.key.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NamespacedKey other = (NamespacedKey) obj;
        return this.namespace.equals(other.namespace) && this.key.equals(other.key);
    }

    @Override
    public String toString() {
        return this.namespace + keySeparator + this.key;
    }
    
    
    private static String[] separateNamespacedString(String namespacedString) {
    	
    	if(!namespacedString.contains(keySeparator)) {
    		throw new IllegalArgumentException("Invalid namespacedString: " + namespacedString);
    	}
    	
    	String[] result = new String[2];
    	
    	int index = namespacedString.indexOf(keySeparator);
    	
    	if(index == namespacedString.length() -1) {
    		throw new IllegalArgumentException("Invalid namespacedString: " + namespacedString);
    	}
    	
    	result[0] = namespacedString.substring(0, index);
    	result[1] = namespacedString.substring(index+1);
    	
		return result;
	}

}