package com.gmail.lJuanGBMinecraft.Quest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gmail.lJuanGBMinecraft.QuestBook.Tab;
import com.gmail.lJuanGBMinecraft.Reward.Reward;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.Task;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

/**
 * Represents a collection of Tasks a player has to complete
 * 
 * @author lJuanGB
 *
 */
public class Quest {
	
	
	public final static String npcKey = "QuestNPC";
	
	/**
	 * A builder pattern to ease the creation of complex quests
	 * 
	 * @author lJuanGB
	 *
	 */
	public final static class Builder {
		
		private Plugin plugin;
		private NamespacedKey ID;
		private boolean legacy = false;
		private String name;
		private String description;
		private Tab tab;
		private ItemStack icon = new ItemStack(Material.PAPER);
		private boolean deleteAtReload = false;
		private boolean repeatable = false;
		private TreeMap<Integer, List<Task>> tasks = new TreeMap<Integer, List<Task>>();
		private List<Reward> rewards = new ArrayList<Reward>();
		
		/**
		 * Start the builder with the most basic information
		 * 
		 * @param plugin The plugin that creates this quest
		 * @param id The unique identifier for this quest. Cannot be repeated within your plugin.
		 */
		public Builder(Plugin plugin, String id) {
            Validate.notNull(plugin, "Plugin cannot be null");
            Validate.notNull(id, "ID cannot be null");

            this.plugin = plugin;
			this.ID = new NamespacedKey(plugin, id);
		}
		
		
		/**
		 * Adds a reward for the quest. See Reward.
		 * 
		 * @param rew
		 * @return
		 */
		public Builder withReward(Reward rew) {
			Validate.notNull(rew, "Reward cannot be null");
			rewards.add(rew);
			
			return this;
		}
		
		/**
		 * Adds a task with a specific order. Players won't be able to complete
		 * tasks with order higher than what they are currently doing.
		 * 
		 * Example:
		 * If I define two tasks with order 3, and one with order 4, and I try
		 * to complete the one with order 4 without having completed BOTH with
		 * order 3, then I won't be able to complete it.
		 * 
		 * NOTE: when the final quest is created, the the order of the tasks will
		 * be reduced to the minimum integers with the same functionality. For example,
		 * if we define the orders 2, 5, 15 they will be converted to 0, 1, 2 respectively.
		 * 
		 * @param order The order as an int. Lowest is 0
		 * @param task
		 * @return
		 */
		public Builder withTask(int order, Task task) {
			Validate.notNull(task, "Task cannot be null");
			if(order < 0) throw new IllegalArgumentException("Order cannot be lower than 0");
			
			if(tasks.get(order) == null) tasks.put(order, new ArrayList<Task>());
			
			tasks.get(order).add(task);
			
			return this;
		}
		
		/**
		 * Adds a task with order 0. See the method withTask(order,task)
		 * 
		 * @param task
		 * @return
		 */
		public Builder withTask(Task task) {
			
			return withTask(0, task);
		}
		
		/**
		 * If all players should automatically cancel this quest at server reload.
		 * 
		 * @param bol
		 * @return
		 */
		public Builder deleteAtReload(boolean bol) {
			this.deleteAtReload = bol;
			
			return this;
		}
		
		/**
		 * If the quest may be re-accepted if it was once completed.
		 * 
		 * @param bol
		 * @return
		 */
		public Builder repeatable(boolean bol) {
			this.repeatable = bol;
			
			return this;
		}
		
		/**
		 * Sets the tab under which the quest will appear in the quest book
		 * 
		 * @param tab
		 * @return
		 */
		public Builder inTab(Tab tab) {
			Validate.notNull(tab, "Tab cannot be null");
			this.tab = tab;
			
			return this;
		}
		
		/**
		 * Sets the icon this mission will have in the quest book
		 * 
		 * @param icon
		 * @return
		 */
		public Builder withIcon(ItemStack icon) {
			Validate.notNull(icon, "Icon cannot be null");
			this.icon = icon;
			
			return this;
			
		}
		
		/**
		 * Sets the icon this mission will have in the quest book
		 * 
		 * @param icon
		 * @return
		 */
		public Builder withIcon(Material icon) {

			return withIcon(new ItemStack(icon));
			
		}
		
		/**
		 * Set the description of this quest.
		 * This will be seen by players when they accept it.
		 * 
		 * @param descriptionPath The path on the language file where this is stored
		 * @return
		 */
		public Builder setDescription(String descriptionPath) {
			Validate.notNull(descriptionPath, "DescriptionPath cannot be null");
			this.description = descriptionPath;
			
			return this;
		}
		
		/**
		 * Set the name of this quest. Does not have to be unique.
		 * This will be seen by players when they accept it.
		 * 
		 * @param namePath The path on the language file where this is stored
		 * @return
		 */
		public Builder setName(String namePath) {
			Validate.notNull(namePath, "NamePath cannot be null");
			this.name = namePath;
			
			return this;
		}
		
		/**
		 * Sets if the quest is legacy (retired from game) or not.
		 * Instead of removing quests from plugin declaration you have
		 * to instead make the quest Legacy.
		 * 
		 * This will tell the plugin to automatically cancel all accepted
		 * quest of this kind and also to not allow accepting it.
		 * 
		 * @param namePath The path on the language file where this is stored
		 * @return
		 */
		public Builder isLegacy(boolean bool) {
			this.legacy = bool;
			
			return this;
		}
		
		/**
		 * 
		 * @return true if the name, description and at least 1 task has been defined
		 */
		public boolean valid() {
						
			if(name == null) return false;
			if(description == null) return false;
			if(tasks.isEmpty()) return false;
			if(tab == null) return false;
			
			return true;
		}
		
		/**
		 * 
		 * @return Quest created from this builder
		 * @throws IllegalArgumentException when the quest is not valid.
		 */
		public Quest build() throws IllegalArgumentException{
			
			return new Quest(this);
		}
	}

	private final Plugin plugin;
	private final NamespacedKey ID;
	private final boolean legacy; 
	private final String name;
	private final String description;
	private final Tab tab;
	private final ItemStack icon;
	private final boolean deleteAtReload;
	private final boolean repeatable;
	private final List< List<Task>> tasks;
	private final List<Reward> rewards;
	
	/**
	 * Creates a class from a builder.
	 * 
	 * The advantage of using the constructor instead of .build() is that one
	 * can create an anonymous class which overrides the automatically called
	 * methods accept(), complete() and cancel()
	 * 
	 * @param builder
	 */
	public Quest(Builder builder) throws IllegalArgumentException {
		
		if(!builder.valid()) {
			
			throw new IllegalArgumentException("QuestBuilder not valid");
		}
		
		List< List<Task> > tempList = new ArrayList< List<Task> >();
		
		for(List<Task> listTask : builder.tasks.values()) {
			tempList.add(Collections.unmodifiableList(listTask));
		}
		
		this.plugin = builder.plugin;
		this.ID = builder.ID;
		this.legacy = builder.legacy;
		this.name = builder.name;
		this.description = builder.description;
		this.tab = builder.tab;
		this.icon = builder.icon;
		this.deleteAtReload = builder.deleteAtReload;
		this.repeatable = builder.repeatable;
		this.tasks = Collections.unmodifiableList(tempList);
		this.rewards = Collections.unmodifiableList(builder.rewards);
	}
	
	/**
	 * Constructs a quest that is a copy of another quest.
	 * 
	 * @param quest
	 */
	public Quest(Quest quest) {
		
		this.plugin = quest.getPlugin();
		this.ID = quest.getID();
		this.legacy = quest.legacy;
		this.name = quest.getNamePath();
		this.description = quest.getDescriptionPath();
		this.tab = quest.getTab();
		this.icon = quest.icon;
		this.deleteAtReload = quest.isDeleteAtReload();
		this.repeatable = quest.repeatable;
		this.tasks = quest.getTasks();
		this.rewards = quest.getRewards();
	}
	
	
	
	public Plugin getPlugin() {
		return plugin;
	}
	public NamespacedKey getID() {
		return ID;
	}
	public boolean isLegacy() {
		return legacy;
	}
	public String getNamePath() {
		return name;
	}
	public String getDescriptionPath() {
		return description;
	}
	public Tab getTab() {
		return tab;
	}
	public ItemStack getIcon() {
		return icon;
	}
	public boolean isDeleteAtReload() {
		return deleteAtReload;
	}
	public boolean isRepeatable() {
		return repeatable;
	}
	
	/**
	 * 
	 * @return An immutable list of [immutable lists of tasks with the same order], 
	 * ordered by the order in which they must completed.
	 */
	public List< List<Task>> getTasks() {
		return tasks;
	}
	
	
	/**
	 * 
	 * @return AN immutable list of the quest rewards
	 */
	public List<Reward> getRewards() {
		return rewards;
	}
	
	/**
	 * 
	 * @return All tasks in the quest, in order of order, but mixed.
	 */
	public List<Task> getAllTasks() {
		
		List<Task> allList = new ArrayList<Task>();
		
		for(List<Task> list : tasks) {
			
			for(Task aTask : list) {
			
			allList.add(aTask);
			}
		}
		
		return allList;
	}
	
	/**
	 * A method left blank called whenever a player accepts this quest.
	 * 
	 * You may extends the Quest class or create anonymous classes to override
	 * this method to include functionality.
	 * 
	 * @param player The player who accepted
	 */
	public void accept(Player player) {}
	
	/**
	 * A method left blank called whenever a player completes this quest.
	 * 
	 * You may extends the Quest class or create anonymous classes to override
	 * this method to include functionality.
	 * 
	 * @param player The player who completed
	 */
	public void complete(Player player) {}
	
	/**
	 * A method left blank called whenever a player cancels this quest.
	 * 
	 * You may extends the Quest class or create anonymous classes to override
	 * this method to include functionality.
	 * 
	 * @param player The player who cancelled
	 */
	public void cancel(Player player) {}
	
	
	/**
	 * 
	 * @param player The player to test.
	 * @return true if player meets all requirements
	 */
	public boolean canPlayerAccept(Player player) {
		
		return getCantAcceptReason(player) == null;
	}
	
	/**
	 * 
	 * @param player
	 * @return Reason why player cannot accept quest. Null if it can be accepted
	 */
	public CantAcceptReason getCantAcceptReason(Player player) {
		
		if(isLegacy()) return CantAcceptReason.LEGACY;
		
		if(PlayerQuestHelper.isQuestAccepted(player, this)) return CantAcceptReason.ACCEPTED;
		
		if(!isRepeatable() && PlayerQuestHelper.isQuestCompleted(player, this)) {
			return CantAcceptReason.COMPLETED;
		}

		
		return null;
	}
	
	
	/**
	 * Rewards a player with all defined quest rewards
	 * 
	 * @param player The player to reward
	 */
	public void rewardPlayer(Player player) {
		
		for(Reward rew : getRewards()) {
			rew.givePlayer(player);
		}
	}
	
	
	/**
	 * Get the order of a task
	 * 
	 * @param task The task to check the order
	 * @return -1 if the task does not belong to this quest
	 */
	public int getOrderOfTask(Task task) {
		
		for(int i = 0; i < tasks.size(); i++) {
			
			if(tasks.get(i).contains(task)) return i;
		}
		
		return -1;
	}
	
	
	/**
	 * 
	 * @param action
	 * @return true if any of this quests task will count this action
	 */
	public boolean shouldActionCount(Action action) {
		
		for(Task task : getAllTasks()) {
			
			if(task.shouldActionCount(action)) return true;
		}
		
		return false;
	}
	
	/**
	 * Called to register this Quest in the QuestsMap.
	 */
	public void register() {
		
		QuestsMap.register(this);
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj == this) return true;
		if(!(obj instanceof Quest)) return false;
		
		Quest quest = (Quest) obj;
		
		if( !this.plugin.equals(quest.getPlugin())) return false;
		if( !this.ID.equals(quest.getID())) return false;
		if( !this.name.equals(quest.getNamePath())) return false;
		if( !this.description.equals(quest.getDescriptionPath())) return false;
		if( !this.tab.equals(quest.getTab())) return false;
		if( !this.icon.equals(quest.getIcon())) return false;
		if( !this.deleteAtReload == quest.isDeleteAtReload()) return false;
		if( !this.tasks.equals(quest.getTasks())) return false;
		if( !this.rewards.equals(quest.getRewards())) return false;
		
		return true;
	}
}
