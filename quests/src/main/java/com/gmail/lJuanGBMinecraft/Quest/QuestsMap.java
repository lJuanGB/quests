package com.gmail.lJuanGBMinecraft.Quest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * Stores all quests registered by all plugins.
 * 
 * @author lJuanGB
 *
 */
public class QuestsMap {

	private static Map<NamespacedKey, Quest> quests = new HashMap<NamespacedKey, Quest>();
	
	
	public static void register(Quest quest) {
		quests.put(quest.getID(), quest);
	}
	
	public static void register(Iterable<Quest> quests) {
		
		for(Quest q : quests) {
			
			register(q);
		}
	}
	
	public static Quest getQuest(NamespacedKey key) {
		return quests.get(key);
	}
	
	public static List<Quest> getQuests(Iterable<NamespacedKey> iterable){
		
		List<Quest> list = new ArrayList<Quest>();
		Iterator<NamespacedKey> keyIterator = iterable.iterator();
		
		while(keyIterator.hasNext()) {
			
			NamespacedKey key = keyIterator.next();
			if(quests.containsKey(key)) list.add( getQuest(key) );
		}
		
		return list;
	}
	
	public static Set<NamespacedKey> getAllKeys(){
		return quests.keySet();
	}
}
