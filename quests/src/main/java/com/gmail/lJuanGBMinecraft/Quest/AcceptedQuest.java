package com.gmail.lJuanGBMinecraft.Quest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageMap;
import com.gmail.lJuanGBMinecraft.Task.AcceptedTask;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.Task;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;
import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;

import net.md_5.bungee.api.ChatColor;

/**
 * Represents a Quest initiated by a Player. Handles taking care of the
 * 
 * @author lJuanGB
 *
 */
public class AcceptedQuest{

	
	private final Quest quest;
	private final List< List<AcceptedTask> > tasks;

	private int completingTasksIndex = 0;
	
	public AcceptedQuest(Quest quest) {
		this.quest = quest;
		
		List< List<AcceptedTask> > tempList = new ArrayList< List<AcceptedTask>>();
		
		for(List<Task> list : quest.getTasks()) {
			
			List<AcceptedTask> aTaskList = new ArrayList<AcceptedTask>();

			for(Task task : list) {
				aTaskList.add(new AcceptedTask(task));
			}
			
			tempList.add(Collections.unmodifiableList(aTaskList));
			
		}
		
		tasks = Collections.unmodifiableList(tempList);
	}
	
	
	public Quest getQuest() {
		return quest;
	}
	
	public boolean isComplete() {
		
		for(List<AcceptedTask> list : tasks) {
			
			for(AcceptedTask aTask : list) {
				
				if(!aTask.isComplete()) return false;
			}
		}

		return true;
	}
	
	
	public List<AcceptedTask> getAllAcceptedTasks() {
		
		List<AcceptedTask> allList = new ArrayList<AcceptedTask>();
		
		for(List<AcceptedTask> list : tasks) {
			
			for(AcceptedTask aTask : list) {
			
			allList.add(aTask);
			}
		}
		
		return allList;
	}
	
	
	public List<AcceptedTask> getCompletedTasks() {
		
		List<AcceptedTask> list = new ArrayList<AcceptedTask>();

		for(AcceptedTask task : getAllAcceptedTasks()) {
			
			if(task.isComplete()) list.add(task);
		}
		
		return list;
	}
	
	public List<AcceptedTask> getCompleteableTasks() {
		
		List<AcceptedTask> list = new ArrayList<AcceptedTask>();

		for(AcceptedTask task : getAllAcceptedTasks()) {
			
			if(!task.isComplete() && quest.getOrderOfTask(task.getTask()) == completingTasksIndex) {
				list.add(task);
			}
		}
		
		return list;
	}
	
	public List<AcceptedTask> getUnCompleteableTasks() {
		
		List<AcceptedTask> list = new ArrayList<AcceptedTask>();

		for(AcceptedTask task : getAllAcceptedTasks()) {
			
			if(!task.isComplete() && quest.getOrderOfTask(task.getTask()) != completingTasksIndex) {
				list.add(task);
			}
		}
		
		return list;
	}
	
	
	public boolean shouldActionCount(Action action) {
		
		for(AcceptedTask task : getCompleteableTasks()) {
			
			if(task.shouldActionCount(action)) return true;
		}
		
		return false;
	}
	
	
	public void sendAction(Action action) {
		
		List<AcceptedTask> tasks = getCompleteableTasks();
		
		for(AcceptedTask aTask : tasks) {
		
			boolean completesTask = aTask.sendAction(action);
			
			if(completesTask) taskCompleted(action.getPlayer(), aTask);
		}
			
	}
	
	public void taskCompleted(Player player, AcceptedTask task) {
		
		PlayerQuestHelper.completeTaskMessage(player, quest);
		
		if(getCompleteableTasks().size() == 0) completingTasksIndex++;
		
		if(isComplete()) PlayerQuestHelper.completeQuest(player, quest, false);
	}
	
	
	public static final String path = "menus.questIcon.";
	public static final String right = "rightClick";
	public static final String left = "leftClick";
	
	public static final int lineLength = 50;
	public static final String indent = "   "; 
	public static final String completed = "\u2714";
	public static final String completeable = "\u25EF";
	public static final String uncompleteable = "\u25EF";
	
	public List<String> getFullDescription(String locale, boolean instructions){
		
		List<String> lore = new ArrayList<String>();
		lore.add("");
		
		String description = LanguageMap.getMessage(getQuest().getPlugin(), 
				locale, getQuest().getDescriptionPath());
		for(String s : UtilMethods.splitStringIntoLines(description, lineLength)) {
			
			lore.add(ChatColor.GRAY + s);
		}
		
		lore.add("");
		
		addTasksToLore(getCompletedTasks(), lore, ChatColor.GREEN, completed, locale);
		addTasksToLore(getCompleteableTasks(), lore, ChatColor.WHITE, completeable, locale);
		addTasksToLore(getUnCompleteableTasks(), lore, ChatColor.GRAY, uncompleteable, locale);

		
		if(instructions) {
			
			lore.add("");
			String left = QuestsAPIPlugin.api.getMessage(locale, path + AcceptedQuest.left);
			String right = QuestsAPIPlugin.api.getMessage(locale, path + AcceptedQuest.right);
			
			lore.add(org.bukkit.ChatColor.GREEN + left);
			lore.add(org.bukkit.ChatColor.RED + right);
		}
		
		return lore;
	}
	
	public ItemStack getFullIcon(String locale, boolean instructions) {
		
		ItemStack icon = getQuest().getIcon().clone();
		
		ItemMeta meta = icon.getItemMeta();
		
		String name = LanguageMap.getMessage(getQuest().getPlugin(), 
				locale, getQuest().getNamePath());
		meta.setDisplayName(ChatColor.WHITE + name);
		
		meta.setLore( getFullDescription(locale, instructions) );
		
		icon.setItemMeta(meta);
		return icon;
	}
	
	
	public static final char openBracket = '(';
	public static final char closeBracket = ')';
	public static final char taskSeparator = ',';

	public String serialize() {
		
		String serial = quest.getID().toString() + openBracket;
		
		for(AcceptedTask task : getAllAcceptedTasks()) {
			
			serial += task.getActionsCompleted() + ",";
		}
		
		serial = serial.substring(0, serial.length() -1) + closeBracket;
		return serial;
	}
	
	
	public static AcceptedQuest deserialize(String serial) throws IllegalArgumentException{
		
		IllegalArgumentException ex = new IllegalArgumentException("Invalid serial: " + serial);
		
		if(StringUtils.countMatches(serial, "" + openBracket) != 1) throw ex;
		if(StringUtils.countMatches(serial, "" + closeBracket) != 1) throw ex;
		if(!serial.contains(taskSeparator + "")) throw ex;
		if(!serial.contains(NamespacedKey.keySeparator + "")) throw ex;
		
		
		
		int openBr = serial.indexOf(openBracket);		
		int closeBr = serial.indexOf(closeBracket);
		
		String[] numbers = serial.substring(openBr +1, closeBr).replaceAll("[^0-9]", " ").split(" ");
		Integer[] actionsCompleted = new Integer[numbers.length];

		for(int i = 0; i < numbers.length; i++) {
			
			actionsCompleted[i] = Integer.parseInt(numbers[i]);
		}
		
		
		String questIDString = serial.substring(0, openBr);
		NamespacedKey questID = new NamespacedKey(questIDString);
		
		Quest quest = QuestsMap.getQuest(questID);
		Validate.notNull(quest, "No quest defined for the key:" + questID.toString());
		
		AcceptedQuest aQuest = new AcceptedQuest(quest);
		
		if(aQuest.getAllAcceptedTasks().size() != actionsCompleted.length) throw ex;
		
		for(int i = 0; i < aQuest.getAllAcceptedTasks().size(); i++) {
			
			aQuest.getAllAcceptedTasks().get(i).setActionCompleted(actionsCompleted[i]);
		}
		
		return aQuest;
	}
	
	
	
	private void addTasksToLore(List<AcceptedTask> tasks, List<String> lore, 
			ChatColor color, String symbol, String locale) {
		
		for(AcceptedTask aTask : tasks) {
			
			String taskDes = aTask.getTask().getDescription(locale);
			String[] splitted = UtilMethods.splitStringIntoLines(taskDes, lineLength - indent.length() - 2);
			
			for(int i = 0; i < splitted.length; i++) {
				
				String loreLine = color + indent;
				if(i == 0) {
					loreLine += symbol + " ";
				}
				
				loreLine += splitted[i];
				lore.add(loreLine);
			}
		}
	}
}
