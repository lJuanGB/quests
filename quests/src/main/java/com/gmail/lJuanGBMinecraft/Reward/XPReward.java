package com.gmail.lJuanGBMinecraft.Reward;

import org.bukkit.entity.Player;

/**
 * Represent a reward in XP amount
 * 
 * @author lJuanGB
 *
 */
public class XPReward implements Reward{

	private final int xp;
	
	public XPReward(int xp) {
		super();
		this.xp = xp;
	}

	public int getXp() {
		return xp;
	}
	
	
	public void givePlayer(Player player) {
		//TODO Implement with MineZ code
	}
	
	
	public String getDescription() {
		
		return xp + " XP.";
	}
}
