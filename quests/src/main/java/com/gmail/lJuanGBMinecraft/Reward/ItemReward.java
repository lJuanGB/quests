package com.gmail.lJuanGBMinecraft.Reward;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;

/**
 * Represents a reward in form of item to be given to the player.
 * 
 * @author lJuanGB
 *
 */
public class ItemReward implements Reward{
	
	private final ItemStack item;
	
	
	public ItemReward(ItemStack item) {

		this.item = item;
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	
	
	@Override
	public void givePlayer(Player player) {
		
		Location loc = player.getLocation();
		
		Item dropItem = loc.getWorld().dropItem(loc, this.item);
		dropItem.setPickupDelay(0);
		dropItem.setVelocity(new Vector(0.0,0.0,0.0));
	}

	@Override
	public String getDescription() {
		
		String description = "";
		
		if(item.getAmount() != 1) {
			
			description += item.getAmount() + " x ";
		}
		
		description += UtilMethods.itemToString(item);
		
		return description;
	}




}
