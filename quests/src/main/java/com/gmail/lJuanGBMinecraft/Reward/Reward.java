package com.gmail.lJuanGBMinecraft.Reward;

import org.bukkit.entity.Player;

/**
 * Represents a reward that will be given to a player if they complete a quest.
 * 
 * @author lJuanGB
 *
 */
public interface Reward {
	
	public void givePlayer(Player player);
	
	/**
	 * 
	 * @return The description of the reward. NOT the path in the language file.
	 * DOES NOT SUPPORT multiple languages.
	 */
	public String getDescription();
}
