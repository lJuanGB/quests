package com.gmail.lJuanGBMinecraft.QuestBook.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.QuestBook.OpenBookMap;

public class InventoryCloseListener implements Listener{

	@EventHandler
	public void onPlayerCloseInventory(InventoryCloseEvent e) {
		 
		if(!(e.getPlayer() instanceof Player)) return;
		
		final Player player = (Player) e.getPlayer();
		
		if(!OpenBookMap.contains(player)) return;
		
		 new BukkitRunnable() {
		        
	            @Override
	            public void run() {
	            	
	            	if(!player.getOpenInventory().getType().equals(InventoryType.CHEST)) {
	            		OpenBookMap.remove(player);
	            	}
	            }
	            
	        }.runTaskLater(QuestsAPIPlugin.getInstance(), 0);
	}
}
