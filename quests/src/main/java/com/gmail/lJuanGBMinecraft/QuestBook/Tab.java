package com.gmail.lJuanGBMinecraft.QuestBook;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageMap;

/**
 * Represents a tab in the quest book 
 * 
 * @author lJuanGB
 *
 */
public class Tab {

	private final Plugin plugin;
	
	private final String id;
	private final ItemStack icon;
	private final String namePath;
	private final Tab parent;
	private int depth = 0;
	private final List<Tab> children = new ArrayList<Tab>();
	
	/**
	 * 
	 * @param icon
	 * @param namePath
	 */
	public Tab(Plugin plugin, String id, ItemStack icon, String namePath, Tab parent) {
		
		this.plugin = plugin;
		this.id = id.toLowerCase().replaceAll(" ", "_");
		this.icon = icon;
		this.namePath = namePath;
		this.parent = parent;
		
		if(parent != null) {
			this.depth = parent.getDepth() + 1;
			parent.addChildren(this);
		}
	}
	
	public Tab(Plugin plugin, String id, ItemStack icon, String namePath) {
		this(plugin, id, icon, namePath, null);
	}

	
	public Plugin getPlugin() {
		return plugin;
	}
	
	public ItemStack getIcon() {
		return icon;
	}

	public String getNamePath() {
		return namePath;
	}

	public String getID() {
		return id;
	}

	public Tab getParent() {
		return parent;
	}
	
	public int getDepth() {
		return this.depth;
	}
	
	public List<Tab> getChildren() {
		return children;
	}
	
	public String getName(Player player) {
		return LanguageMap.getMessage(plugin, player, namePath);
	}
	
	public String getName(String locale) {
		return LanguageMap.getMessage(plugin, locale, namePath);
	}
	
	
	public boolean isRoot() {
		return this.parent == null;
	}
	
	public ItemStack getNamedIcon(String locale) {
		
		String itemName = LanguageMap.getMessage(this.plugin, locale, namePath);
		
		ItemStack result = icon.clone();
		ItemMeta meta = result.getItemMeta();
		meta.setDisplayName(ChatColor.WHITE + itemName);
		
		result.setItemMeta(meta);
		return result;
	}
	
	private void addChildren(Tab tab) {
		this.children.add(tab);
	}
	

}
