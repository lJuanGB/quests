package com.gmail.lJuanGBMinecraft.QuestBook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

public class OpenBookMap {

	private static Map<UUID, OpenQuestBook> books = new HashMap<UUID, OpenQuestBook>();
	
	
	public static void register(OpenQuestBook book) {
		books.put(book.getPlayer().getUniqueId(), book);
	}
	
	public static OpenQuestBook getOpenQuestBook(Player key) {
		return getOpenQuestBook(key.getUniqueId());
	}
	
	public static OpenQuestBook getOpenQuestBook(UUID uuid) {
		return books.get(uuid);
	}
	
	public static List<OpenQuestBook> getOpenQuestBooks(Iterable<Player> iterable){
		
		List<OpenQuestBook> list = new ArrayList<OpenQuestBook>();
		Iterator<Player> keyIterator = iterable.iterator();
		
		while(keyIterator.hasNext()) {
			
			UUID key = keyIterator.next().getUniqueId();
			if(books.containsKey(key)) list.add( getOpenQuestBook(key) );
		}
		
		return list;
	}
	
	public static boolean contains(Player player) {
		return books.containsKey(player.getUniqueId());
	}
	
	public static void remove(Player player) {
		books.remove(player.getUniqueId());
	}
}