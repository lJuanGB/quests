package com.gmail.lJuanGBMinecraft.QuestBook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageAPI;
import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class OpenQuestBook {

	public static final String path = "menus.questBook.";
	public static final String back = "back";
	public static final String title = "title";

	
	private final Player opener;
	
	private final Map<Tab, List<Tab>> tabs = new HashMap<Tab, List<Tab>>();
	private final Map<Tab, List<AcceptedQuest>> quests = new HashMap<Tab, List<AcceptedQuest>>();
	
	
	private Tab currentTab = null;
	private final List<Object> itemsInInventory = new ArrayList<Object>();
	
	
	public OpenQuestBook(Player opener) {
		
		this.opener = opener;
		
		storeQuests(opener);
	}

	
	public void openTab(Tab tab) {
		
		List<Tab> tabs = this.tabs.containsKey(tab) ? this.tabs.get(tab) : new ArrayList<Tab>();
		List<AcceptedQuest> quests = this.quests.containsKey(tab) ? 
				this.quests.get(tab) : new ArrayList<AcceptedQuest>();

		int totalElements = tabs.size() + quests.size();
		
		int invSize = (int) (9 * Math.ceil( (totalElements + 1.0)/9 ));
		String invName = getInventoryName(tab);
		Inventory inv = Bukkit.createInventory(null, invSize, invName);
		
		String locale = LanguageAPI.getLocale(opener);
		
		itemsInInventory.clear();
		
		for(Tab tabLoop : tabs) {
			
			itemsInInventory.add(tabLoop);
			inv.addItem(tabLoop.getNamedIcon(locale));
		}
		
		for(AcceptedQuest questLoop : quests) {
			
			itemsInInventory.add(questLoop);
			inv.addItem(questLoop.getFullIcon(locale, true));
		}
		
		if(tab != null) {
			
			inv.setItem(invSize - 1, getBackIcon());
		}
		
		currentTab = tab;
		opener.openInventory(inv);
	}
	
	public Tab getCurrentTab() {
		return currentTab;
	}
	
	public Player getPlayer() {
		return opener;
	}
	
	
	public List<Object> getItemsInInventory() {
		return itemsInInventory;
	}



	public static final String connector = " \u27A4 ";
	
	private String getInventoryName(Tab tab) {
		
		String name = QuestsAPIPlugin.api.getMessage(opener, path + title);
		name = name + getTabNameConnected(tab, "");
		return name;
	}


	private String getTabNameConnected(Tab tab, String string) {

		if(tab == null) return string;
		
		string = connector + tab.getName(opener) + string;
		
		return getTabNameConnected(tab.getParent(), string);
	}


	public ItemStack getBackIcon() {
		
		ItemStack icon = new ItemStack(Material.EMERALD);
		
		ItemMeta meta = icon.getItemMeta();
		meta.setDisplayName(QuestsAPIPlugin.api.getMessage(opener, path + back));
		
		icon.setItemMeta(meta);
		return icon;
	}

	private void storeQuests(Player player) {

		List<AcceptedQuest> aQuests = PlayerQuestHelper.getAcceptedQuests(player);
		
		for(AcceptedQuest quest : aQuests) {
			
			Tab tab = quest.getQuest().getTab();
			
			if(!quests.containsKey(tab)) quests.put(tab, new ArrayList<AcceptedQuest>());
			
			quests.get(tab).add(quest);
			
			storeTab(tab);
		}
	}
	
	private void storeTab(Tab tab) {
		
		if(tab == null) return;
		
		Tab parent = !tab.isRoot() ? tab.getParent() : null;
		
		if(!tabs.containsKey(parent)) tabs.put(parent, new ArrayList<Tab>());
		
		tabs.get(parent).add(tab);
		
		storeTab(parent);
	}
	
}
