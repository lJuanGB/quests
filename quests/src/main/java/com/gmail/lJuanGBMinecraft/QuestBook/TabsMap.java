package com.gmail.lJuanGBMinecraft.QuestBook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TabsMap {

	private static Map<String, Tab> tabs = new HashMap<String, Tab>();
	
	
	public static void register(Tab tab) {
		tabs.put(tab.getID(), tab);
	}
	
	public static Tab getTab(String key) {
		return tabs.get(key.toLowerCase().replaceAll(" ", "_"));
	}
	
	public static List<Tab> getTabs(Iterable<String> iterable){
		
		List<Tab> list = new ArrayList<Tab>();
		Iterator<String> keyIterator = iterable.iterator();
		
		while(keyIterator.hasNext()) {
			
			String key = keyIterator.next();
			if(tabs.containsKey(key)) list.add( getTab(key) );
		}
		
		return list;
	}
}
