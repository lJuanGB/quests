package com.gmail.lJuanGBMinecraft.QuestBook.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageAPI;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageMap;
import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.QuestBook.OpenBookMap;
import com.gmail.lJuanGBMinecraft.QuestBook.OpenQuestBook;
import com.gmail.lJuanGBMinecraft.QuestBook.Tab;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class BookClickListener implements Listener{

	@EventHandler
	public void onPlayerClick(InventoryClickEvent e) {
		
		if(!(e.getWhoClicked() instanceof Player)) return;
		
		Player player = (Player) e.getWhoClicked();
		
		if(!OpenBookMap.contains(player)) return;
		
		OpenQuestBook book = OpenBookMap.getOpenQuestBook(player);
		int clicked = e.getRawSlot();
		
		if(book.getItemsInInventory().size() > clicked && clicked >= 0) {
			
			Object obj = book.getItemsInInventory().get(clicked);
			
			if(obj instanceof Tab) {
				
				Tab tab = (Tab) obj;
				book.openTab(tab);
			}
			
			if(obj instanceof AcceptedQuest) {
				
				AcceptedQuest aQuest = (AcceptedQuest) obj;
				
				if(e.isLeftClick()) {
					
					String locale = LanguageAPI.getLocale(player);
					
					String name = LanguageMap.getMessage(aQuest.getQuest().getPlugin(), locale, aQuest.getQuest().getNamePath());
					player.sendMessage("");
					player.sendMessage(name);
					
					String[] mes = aQuest.getFullDescription(locale, false)
											.toArray(new String[0]);
					player.sendMessage(mes);
				}
				
				if(e.isRightClick()) {
					
					PlayerQuestHelper.cancelQuest(player, aQuest.getQuest(), false);
					player.closeInventory();
				}
			}
			
		} else if(
				e.getCurrentItem() != null
				&& e.getCurrentItem().equals(book.getBackIcon())
				) 
		{
			
			book.openTab( book.getCurrentTab().getParent() );
		}
		
		e.setCancelled(true);
	}
}
