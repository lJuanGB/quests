package com.gmail.lJuanGBMinecraft.API;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageMap;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Quest.NamespacedKey;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Quest.QuestsMap;
import com.gmail.lJuanGBMinecraft.QuestBook.OpenQuestBook;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class QuestsAPI {

	private final Plugin plugin;
	
	public QuestsAPI(Plugin plugin) {
		super();
		this.plugin = plugin;
	}

	/**
	 * 
	 * @return The plugin used for this QuestApi
	 */
	public Plugin getPlugin() {
		return plugin;
	}
	
	
	/**
	 * Constructs a Builder so that the quest created by it will
	 * automatically be registered with your plugin. Quests are
	 * registered with NamespacedKeys ("plugin:id").
	 * 
	 * @param id The Unique Identifier for your quest
	 * @return A builder to ease the creation of quests.
	 */
	public Quest.Builder getQuestBuilder(String id){
		
		return new Quest.Builder(plugin, id);
	}
	
	
	/**
	 * If you want a quest declared by your plugin, you may use
	 * getQuest(String id).
	 * 
	 * @param key 
	 * @return The quest registered with the specified key
	 */
	public Quest getQuest(NamespacedKey key) {
		
		return QuestsMap.getQuest(key);
	}
	
	/**
	 * If you want a quest declared by another plugin, you have
	 * to use getQuest(NamespacedKey key).
	 * 
	 * @param id The id of the quest
	 * @return The quest registered with the specified id in your plugin
	 */
	public Quest getQuest(String id) {
		
		NamespacedKey key = new NamespacedKey(plugin, id);
		return getQuest(key);
	}
	
	
	
	
	/**
	 * Useful for obtaining a message from your plugin specifying the locale.
	 * 
	 * @param locale The locale in format ISO 639-1 and ISO 3166-2. ("en_US")
	 * @param path The path in the language file of your plugin.
	 * @return The message that would be generated from that path and locale.
	 */
	public String getMessage(String locale, String path) {
		
		return LanguageMap.getMessage(plugin, locale, path);
	}
	
	/**
	 * Useful for obtaining a message from your plugin specifying the locale.
	 * 
	 * @param locale The locale in format ISO 639-1 and ISO 3166-2. ("en_US")
	 * @param path The path in the language file of your plugin.
	 * @param variables The LanguageVariables that will be applied. See LanguageVariable.
	 * @return The message that would be generated from that path and locale.
	 */
	public String getMessage(String locale, String path, List<LanguageVariable> variables) {
		
		return LanguageMap.getMessage(plugin, locale, path, variables);
	}
	
	/**
	 * Useful for obtaining a message from a plugin specifying the locale.
	 * 
	 * @param plugin The plugin from whose language files to search for the path.
	 * @param locale The locale in format ISO 639-1 and ISO 3166-2. ("en_US")
	 * @param path The path in the language file of the plugin specified.
	 * @return The message that would be generated from that path and locale.
	 */
	public String getMessage(Plugin plugin, String locale, String path) {
		
		return LanguageMap.getMessage(plugin, locale, path);
	}
	
	/**
	 * Useful for obtaining a message from a plugin specifying the locale.
	 * 
	 * @param plugin The plugin from whose language files to search for the path.
	 * @param locale The locale in format ISO 639-1 and ISO 3166-2. ("en_US")
	 * @param path The path in the language file of the plugin specified.
	 * @param variables The LanguageVariables that will be applied. See LanguageVariable.
	 * @return The message that would be generated from that path and locale.
	 */
	public String getMessage(Plugin plugin, String locale, String path, List<LanguageVariable> variables) {
		
		return LanguageMap.getMessage(plugin, locale, path, variables);
	}
	
	/**
	 * Used to obtain a player locale. It does not matter if said locale is
	 * supported by the plugin or not.
	 * 
	 * @param player The player from which to extract the locale.
	 * @return The locale found.
	 */
	public String getLocale(Player player) {
		
		return LanguageMap.getLocale(player);
	}
	
	/**
	 * 
	 * @return The default language that every plugin should support.
	 */
	public String getDefaultLocale() {
		return LanguageMap.getDefaultLocale();
	}
	
	
	/**
	 * Forces a player to open the QuestBook as if the had input
	 * the command /questbook.
	 * 
	 * @param player
	 */
	public void openQuestBook(Player player) {
		
		OpenQuestBook book = new OpenQuestBook(player);
		book.openTab(null);
	}
	
	
	/**
	 * 
	 * @param player 
	 * @return An unmodifiable list of all accepted task quests.
	 */
	public List<AcceptedQuest> getAcceptedQuests(Player player){
		return PlayerQuestHelper.getUnmodifiableAcceptedQuests(player);
	}
	
	/**
	 * 
	 * @param player
	 * @param quest
	 * @return true if the player has accepted and is currently accepted
	 * the quest. False otherwise.
	 */
	public boolean isQuestAccepted(Player player, Quest quest) {
		return PlayerQuestHelper.isQuestAccepted(player, quest);
	}
	
	public AcceptedQuest getAcceptedQuest(Player player, Quest quest) {
		return PlayerQuestHelper.getAcceptedQuest(player, quest);
	}
	
	/**
	 * 
	 * @param player
	 * @return An unmodifiable quest with all quests ever completed by the player.
	 */
	public List<Quest> getCompletedQuests(Player player){
		
		List<Quest> result = new ArrayList<Quest>();
		
		for(NamespacedKey key : PlayerQuestHelper.getCompletedQuestsKeys(player)) {
			
			result.add(QuestsMap.getQuest(key));
		}
		
		return Collections.unmodifiableList(result);
	}
	
	/**
	 * 
	 * @param player
	 * @param quest
	 * @return True if the player ever completed the quest. False otherwise.
	 */
	public boolean isQuestCompleted(Player player, Quest quest) {
		return PlayerQuestHelper.isQuestCompleted(player, quest);
	}
	
	
	
	/**
	 * "Give" the player a quest. If the player doesn't meet the requirements,
	 * the quest is already accepted or it was completed and is not repeatable,
	 * then the quest is not accepted by the player.
	 * 
	 * @param player
	 * @param quest
	 * @return true if the quest was finally accepted, false otherwise
	 */
	public boolean giveQuest(Player player, Quest quest) {
		return PlayerQuestHelper.giveQuest(player, quest, false);
	}
	
	/**
	 * Forces the player to abandon the quest.
	 * Won't do anything if the quest has not been accepted.
	 * 
	 * @param player The player
	 * @param quest The quest
	 * @param silently Whether it should not send the player messages
	 * @return false if the quest was not previously accepted or the QuestCancelEvent was cancelled.
	 */
	public boolean cancelQuest(Player player, Quest quest) {
		return PlayerQuestHelper.cancelQuest(player, quest, false);
	}
	
	/**
	 * Artificially completes the quest for the player.
	 * Won't do anything if the quest has not been accepted.
	 * 
	 * @param player The player
	 * @param quest The quest
	 * @param silently Whether it should not send the player messages
	 * @return false if the quest was not previously accepted.
	 */
	public boolean completeQuest(Player player, Quest quest) {
		return PlayerQuestHelper.completeQuest(player, quest, false);
	}
}
