package com.gmail.lJuanGBMinecraft.API;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageAPI;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Quest.QuestsMap;

public abstract class QuestPlugin extends JavaPlugin{

	@Override
	public final void onEnable() {
		
		LanguageAPI.loadLocales(this, getSupportedLocales());
		
		Iterable<Quest> quests = getQuests();
		if(quests != null) {
			QuestsMap.register(quests);
		}
		
		onStart();
	}
	
	
	/**
	 * Equivalent to the onEnable() for normal JavaPlugins.
	 * Will be run after the languages and quests are generated and
	 * registered.
	 */
	public void onStart() {};
	
	/**
	 * All quests that your plugin will define should be returned by
	 * this method. Creating and registering quests not at the same time
	 * the plugin is enabled, although possible, will most likely cause
	 * too many errors.
	 * 
	 * Run after the languages are loaded.
	 * 
	 * @return An iterable with all quests to define.
	 */
	public abstract Iterable<Quest> getQuests();
	
	/**
	 * All locales this plugin supports should be returned by this
	 * method.
	 * 
	 * Locales should follow the format ISO 639-1 and ISO 3166-2. ("en_US")
	 * 
	 * This method is run first when the plugin is enabled.
	 * 
	 * @return A string array with all supported locales.
	 */
	public abstract String[] getSupportedLocales();
	
	
	
	/**
	 * 
	 * @return An API with the most useful methods to define quests and
	 * perform basic but useful operations with quests.
	 */
	public QuestsAPI getAPI() {
		
		return new QuestsAPI(this);
	}
}
