package com.gmail.lJuanGBMinecraft.API.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * A java list wrapper that facilitates getting random objects from the list by using
 * defined chances.
 * 
 * @author lJuanGB
 *
 * @param <T> The class of the object that the list will be composed of
 */
public class RandomList<T> {

	public static class Element<T>{
		
		private double chance;
		private T element;
		
		
		public Element(double chance, T element) {
			super();
			this.chance = chance;
			this.element = element;
		}
		
		
		public double getChance() {
			return chance;
		}
		public void setChance(double chance) {
			this.chance = chance;
		}
		public T getElement() {
			return element;
		}
		public void setElement(T element) {
			this.element = element;
		}
		
		
	}
	
	
	private final Random rand = new Random();
	private final List<Element<T>> elements = new ArrayList<Element<T>>();
	
	
	
	public RandomList(Iterable<Element<T>> elements) {
		
		for(Element<T> el : elements) {
			
			this.elements.add(el);
		}
	}
	
	public RandomList(@SuppressWarnings("unchecked") Element<T>...elements ) {
		this(Arrays.asList( elements ));
	}

	
	
	public List<Element<T>> getElements() {
		return elements;
	}
	
	
	public void add(Element<T> el) {
		elements.add(el);
	}
	
	
	/**
	 * If we have declared element A with chance 0.3, element B
	 * with chance 0.7, and element C with chance 1, then
	 * this method will return a list that has a 30% chance 
	 * of containing A, a 70% chance of containing B and a
	 * 100% chance of containing C.
	 * 
	 * Thus, the size of the return list cannot be known.
	 * 
	 * @return
	 */
	public List<T> getPureRandom(){
		
		List<T> result = new ArrayList<T>();
		
		for(Element<T> el : elements) {
			
			if(el.getChance() > rand.nextDouble()) {
				
				result.add(el.getElement());
			}
		}
		
		return result;
	}
	
	
	/**
	 * If we have declared element A with chance 0.3, element B
	 * with chance 0.7, and element C with chance 1, then A is
	 * return with 30% probability. If A is not returned, then
	 * B is returned with 70% probability. If B is not returned,
	 * then C is returned with 100% probability.
	 * 
	 * The real probabilities for this method to return A is 30%,
	 * B is 49% ( (1 - 0.3) * 0.7) and C is 21% ( (1-0.49 -0.3) * 1)
	 * 
	 * @return May return null if no element has a chance of 1.
	 */
	public T getStairRandom() {
		
		for(Element<T> el : elements) {
			
			if(el.getChance() > rand.nextDouble()) {
				
				return el.getElement();
			}
		}
		
		return null;
	}
	
	
	/**
	 * Returns a list with elements corresponding of running getStairRandom()
	 * n times.
	 * 
	 * The size may not be determined, since null results are not added to this
	 * list.
	 * 
	 * @param n
	 * @return
	 */
	public List<T> getStairRandom(int n){
		
		List<T> result = new ArrayList<T>();

		for(int i = 0; i < n; i++) {
			
			T t = getStairRandom();
			
			if(t != null) result.add(t);
		}
		
		return result;
	}
	
	/**
	 * Returns a list with n elements filled by using getStairRandom()
	 * until the size is achieved.
	 * 
	 * @param n
	 * @return
	 */
	public List<T> getFullStairRandom(int n){
		
		List<T> result = new ArrayList<T>();

		while(result.size() < n) {
			
			T t = getStairRandom();
			
			if(t != null) result.add(t);
		}
		
		return result;
	}
	
	
	/**
	 * The chances are weighted over 1 and then a random element
	 * is chosen. The order in which the elements were declared
	 * doesn't matter. If A has chance 2, B has chance 1 and
	 * C has chance 3, then A has a chance of coming out of this
	 * method of 1/3, B of 1/6 and C of 1/2.
	 * 
	 * @return
	 */
	public T getRandom() {
		
		List<double[]> chanceIntervals = getChanceInterval();
		
		double randomDouble = rand.nextDouble();
		
		for(int i = 0; i < elements.size() ; i++) {
			if(randomDouble > chanceIntervals.get(i)[0] &&
			   randomDouble < chanceIntervals.get(i)[1]) {
				
				return elements.get(i).getElement();
			}
		}
		
		return null;
	}
	
	/**
	 * Return a list of size n filled with elements chosen
	 * by doing getRandom() n times.
	 * 
	 * The size of this list is always n.
	 * 
	 * @param n
	 * @return
	 */
	public List<T> getRandom(int n){
	
		List<T> result = new ArrayList<T>();

		while(result.size() < n) {
			
			T t = getRandom();
			
			if(t != null) result.add(t);
		}
		
		return result;
	}
	
	
	
	
	private List<double[]> getChanceInterval() {

		List<double[]> chanceIntervals = new ArrayList<double[]>();
		
		double totalChance = 0;
		for(Element<T> el : elements) {
			
			totalChance += el.getChance();
		}
		
		
		double previousChance = 0;
		
		for(Element<T> el : elements) {
			
			double weighted = el.getChance() / totalChance;
			chanceIntervals.add(new double[] {previousChance, previousChance + weighted});
			previousChance += weighted;
		}
		
		return chanceIntervals;
	}
}
