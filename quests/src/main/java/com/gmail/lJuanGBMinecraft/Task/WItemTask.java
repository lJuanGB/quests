package com.gmail.lJuanGBMinecraft.Task;

import org.bukkit.inventory.ItemStack;

/**
 * A task that may only accept actions done with a specific item in hand
 * 
 * @author lJuanGB
 *
 */
public interface WItemTask {

	public boolean isItemValid(ItemStack item);
}
