package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Talk;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.DialogerAPI.NPC.DialogerNPC;
import com.gmail.lJuanGBMinecraft.NPC.CompleteTalkTaskDialog;
import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * Given to players who reach the given dialog in a conversation with the specified NPC.
 * 
 * @author lJuanGB
 *
 */
public class TalkAction extends Action{

	private final DialogerNPC npc;
	private final CompleteTalkTaskDialog dialog;
	
	public TalkAction(Player player, CompleteTalkTaskDialog dialog, DialogerNPC npc) {
		super(player);
		
		this.dialog = dialog;
		this.npc = npc;
	}

	
	public CompleteTalkTaskDialog getDialog() {
		return dialog;
	}


	public DialogerNPC getNpc() {
		return npc;
	}

}
