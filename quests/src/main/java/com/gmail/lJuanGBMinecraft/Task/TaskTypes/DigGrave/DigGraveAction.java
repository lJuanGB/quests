package com.gmail.lJuanGBMinecraft.Task.TaskTypes.DigGrave;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * Action given when a player digs a grave
 * 
 * @author lJuanGB
 *
 */
public class DigGraveAction extends Action{

	public DigGraveAction(Player player) {
		super(player);
	}

}
