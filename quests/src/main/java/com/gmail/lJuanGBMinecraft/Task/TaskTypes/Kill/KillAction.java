package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Kill;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * An action given to players who kill mobs or other players.
 * 
 * @author lJuanGB
 *
 */
public class KillAction extends Action{

	private final String monsterName;
	private final MobType monType;
	
	public KillAction(Player player, Entity entity) {
		super(player);

		monsterName = entity.getCustomName();
		monType = MobType.getType(entity);
	}

	public String getMonsterName() {
		return monsterName;
	}

	public MobType getMonType() {
		return monType;
	}

	
	
}
