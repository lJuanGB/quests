package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Loot;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * A task where a player has to loot an amount of chests.
 * May or may not require them to be a specific type.
 * 
 * @author lJuanGB
 *
 */
public class LootTask extends APITask{

	public static final String path = "generic.task.loot.";
	
	public static final String numberKey = "number";
	public static final String chestIdKey = "chestId";
	
	public static final String oneChest = "oneChest";
	public static final String multipleChests = "multipleChests";
	public static final String oneIDChest = "oneIDChest";
	public static final String multipleIDChests = "multipleIDChests";
	
	
	
	private final String chestID;
	
	
	public LootTask(String forcedDescription, int actionGoal, String chestID) {
		super(forcedDescription, actionGoal);
	
		this.chestID = chestID;
	}
	
	public LootTask(int actionGoal, String chestID) {
		this(null, actionGoal, chestID);
	}
	
	public LootTask(String forcedDescription, int actionGoal) {
		this(forcedDescription, actionGoal, null);
	}
	
	public LootTask(int actionGoal) {
		this(null, actionGoal, null);
	}

	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(getChestID() == null) {
			
			if(super.getActionGoal() == 1) {
				
				return path + oneChest;
				
			} else {
				
				return path + multipleChests;
			}
			
		} else {
			
			if(super.getActionGoal() == 1) {
				
				return path + oneIDChest;
				
			} else {
				
				return path + multipleIDChests;
			}
		}

	}

	
	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		String chestName;
		if(chestID == null) {
			chestName = "";
		} else {
			chestName = WordUtils.capitalizeFully(chestID).replaceAll("_", " ");
		}
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(numberKey, super.getActionGoal() + ""));
		variables.add(new LanguageVariable(chestIdKey, chestName));
		
		return variables;
	}

	@Override
	public boolean shouldActionCount(Action action) {
		if(!(action instanceof LootAction)) return false;
		
		LootAction lAction = (LootAction) action;
		
		if(chestID == null) return true;
		
		return lAction.getChestID().equals(chestID);
	}

	
	public String getChestID() {
		return chestID;
	}	
	
}
