package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Loot;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.LocalizedTask;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * A task where a player has to loot an amount of chests in a region.
 * 
 * @author lJuanGB
 *
 */
public class LocalizedLootTask extends LootTask implements LocalizedTask{

	public static final String path = "generic.task.loot.localized.";
	public static final String regionNameKey = "regionName";
	
	
	
	private final Region region;
	

	public LocalizedLootTask(String forcedDescription, int actionGoal, String chestID, Region region) {
		super(forcedDescription, actionGoal, chestID);

		this.region = region;
	}
	
	public LocalizedLootTask(int actionGoal, String chestID, Region region) {
		this(null, actionGoal, chestID, region);
	}

	public LocalizedLootTask(int actionGoal, Region region) {
		this(null, actionGoal, null, region);
	}


	public LocalizedLootTask(String forcedDescription, int actionGoal, Region region) {
		this(forcedDescription, actionGoal, null, region);
	}

	
	
	
	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(getChestID() == null) {
			
			if(super.getActionGoal() == 1) {
				
				return path + oneChest;
				
			} else {
				
				return path + multipleChests;
			}
			
		} else {
			
			if(super.getActionGoal() == 1) {
				
				return path + oneIDChest;
				
			} else {
				
				return path + multipleIDChests;
			}
		}

	}
	

	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = super.getGeneratedVariables(locale);
		variables.add(new LanguageVariable(regionNameKey, region.getRegionName()));

		return variables;
	}

	
	
	@Override
	public boolean shouldActionCount(Action action) {
		
		if( !isInRange(action.getPosition(), action.getWorld()) ) return false;
				
		return super.shouldActionCount(action);
	}
	
	
	
	@Override
	public boolean isInRange(Vector position, String world) {
		return region.isInside(position, world);
	}

	@Override
	public Region getRegion() {
		return region;
	}
	
	@Override
	public boolean isInRange(Location loc) {
		return region.isInside(loc);
	}
}
