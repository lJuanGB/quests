package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Defend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

/**
 * Stores all defenses registered by all plugins.
 * 
 * @author lJuanGB
 *
 */
public class DefenseMap {

	private static Map<UUID, DefenseInstance> defenses = new HashMap<UUID, DefenseInstance>();
	
	
	public static void register(DefenseInstance defense) {
		defenses.put(defense.getPlayer().getUniqueId(), defense);
	}
	
	public static DefenseInstance getDefenseInstance(Player key) {
		return defenses.get(key.getUniqueId());
	}
	
	public static DefenseInstance getDefenseInstance(UUID uuid) {
		return defenses.get(uuid);
	}
	
	public static List<DefenseInstance> getDefenseInstances(Iterable<Player> iterable){
		
		List<DefenseInstance> list = new ArrayList<DefenseInstance>();
		Iterator<Player> keyIterator = iterable.iterator();
		
		while(keyIterator.hasNext()) {
			
			UUID key = keyIterator.next().getUniqueId();
			if(defenses.containsKey(key)) list.add( getDefenseInstance(key) );
		}
		
		return list;
	}
	
	public static boolean contains(Player player) {
		return defenses.containsKey(player.getUniqueId());
	}
}