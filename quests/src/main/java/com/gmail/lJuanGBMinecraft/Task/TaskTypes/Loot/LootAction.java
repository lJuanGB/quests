package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Loot;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * Given when a player loots a chest
 * 
 * @author lJuanGB
 *
 */
public class LootAction extends Action{

	private final String chestID;
	
	public LootAction(Player player, String chestID) {
		super(player);
		
		this.chestID = chestID;
	}

	
	public String getChestID() {
		return chestID;
	}

}
