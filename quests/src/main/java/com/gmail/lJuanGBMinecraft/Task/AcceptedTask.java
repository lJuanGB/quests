package com.gmail.lJuanGBMinecraft.Task;

import org.bukkit.Bukkit;

import com.gmail.lJuanGBMinecraft.Events.ActionCountEvent;
import com.gmail.lJuanGBMinecraft.Events.TaskCompleteEvent;

/**
 * Represents an accepted instance of a class. This keeps track of how many
 * actions have been completed already.
 * 
 * @author lJuanGB
 *
 */
public class AcceptedTask {

	private final Task task;
	private int actionsCompleted = 0;
	
	
	public AcceptedTask(Task task) {
		this.task = task;
	}


	public Task getTask() {
		return task;
	}


	public int getActionsCompleted() {
		return actionsCompleted;
	}
	
	public void setActionCompleted(int i) {
		this.actionsCompleted = i;
	}
	
	
	
	public double getCompletion() {
		return ((getActionsCompleted() + 0.0) / (getTask().getActionGoal() + 0.0));
	}
	
	
	public int getCompletionPercentage() {
		
		return (int) Math.round(getCompletion() * 100);
	}
	
	
	public boolean isComplete() {
		
		return (getActionsCompleted() >= getTask().getActionGoal());
	}
	
	
	public boolean shouldActionCount(Action action) {
		
		return getTask().shouldActionCount(action);
	}
	
	/**
	 * Receives an action, checks if the action should count towards the task and if so the
	 * action count increases.
	 * 
	 * Calls the ActionCountEvent, which may be cancelled.
	 * 
	 * @param action The action
	 * @return true if the task would is completed after the method is called.
	 */
	public boolean sendAction(Action action) {
		
		if(!shouldActionCount(action)) return false;
		
		if(isComplete()) return false;		
		
		ActionCountEvent aEvent = new ActionCountEvent(action.getPlayer(), this, action);
		Bukkit.getPluginManager().callEvent(aEvent);
		
		if(aEvent.isCancelled()) return false;
		
		
		actionsCompleted++;
		getTask().actionCounted(action);
		
		if(isComplete()) {
			TaskCompleteEvent tEvent = new TaskCompleteEvent(action.getPlayer(), this);
			Bukkit.getPluginManager().callEvent(tEvent);
			
			return true;
		}
		
		return false;
	}
}
