package com.gmail.lJuanGBMinecraft.Task.TaskTypes.RegionMovement;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.LocalizedTask;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.RegionMovement.RegionMovementAction.MovementType;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * A task where a player has to join or leave a region.
 * 
 * @author lJuanGB
 *
 */
public class RegionMovementTask extends APITask implements LocalizedTask{

	public static final String path = "generic.task.region.";
	
	public static final String regionNameKey = "regionName";
	
	public static final String join = "join";
	public static final String leave = "leave";
	
	
	private final Region region;
	private final MovementType movementType;
	
	
	public RegionMovementTask(String forcedDescription, Region region, MovementType movementType) {
		super(forcedDescription, 1);
		
		this.region = region;
		this.movementType = movementType;
	}
	
	public RegionMovementTask(Region region, MovementType movementType) {
		this(null, region, movementType);
	}

	
	
	public Region getRegion() {
		return region;
	}

	public MovementType getMovementType() {
		return movementType;
	}
	
	
	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(movementType.equals(MovementType.JOIN)) {
			
			return path + join;	
		}
			
		if(movementType.equals(MovementType.LEAVE)) {
			
			return path + leave;	
		}
		
		return null;
	}
	

	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(regionNameKey, region.getRegionName()));

		return variables;
	}

	@Override
	public boolean shouldActionCount(Action action) {
		
		if( !(action instanceof RegionMovementAction) ) return false;
				
		RegionMovementAction rAction = (RegionMovementAction) action;
		
		if(!rAction.getRegion().equals(region)) return false;
		
		return true;
	}



	@Override
	public boolean isInRange(Vector position, String world) {
		return region.isInside(position, world);
	}
	
	@Override
	public boolean isInRange(Location loc) {
		return region.isInside(loc);
	}
}
