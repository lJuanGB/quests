package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Deliver;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.bukkit.inventory.ItemStack;

import com.gmail.lJuanGBMinecraft.DialogerAPI.NPC.DialogerNPC;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.NPC.CompleteTalkTaskDialog;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.Talk.TalkTask;
import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;

/**
 * A task where a player has to deliver some items to a NPC.
 * 
 * @author lJuanGB
 *
 */
public class DeliverTask extends APITask{

	public static final String path = "generic.task.deliver.";
	
	public static final String itemsKey = "items";
	
	public static final String show = "show";
	public static final String deliver = "deliver";
	
	
	private final ItemStack[] items;
	private final CompleteTalkTaskDialog dialog;
	private final DialogerNPC npc;
	
	public DeliverTask(String forcedDescription, CompleteTalkTaskDialog dialog, DialogerNPC npc, ItemStack... items) {
		super(forcedDescription, 1);
		
		this.items = items;
		this.npc = npc;
		this.dialog = dialog;
	}
	
	public DeliverTask(CompleteTalkTaskDialog dialog, DialogerNPC npc, ItemStack... items) {
		this(null, dialog, npc, items);
	}



	public ItemStack[] getItems() {
		return items;
	}



	@Override
	public String getGeneratedDescriptionPath() {
		
		return path + deliver;
	}




	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(itemsKey, itemsToString(items)));
		variables.add(new LanguageVariable(TalkTask.nameNPCKey, npc.getName()));
		
		return variables;
	}




	@Override
	public boolean shouldActionCount(Action action) {
		
		if(!(action instanceof DeliverAction)) return false;
		
		DeliverAction dAction = (DeliverAction) action;
		
		if(!dAction.getDialog().isSimilar(dialog)) return false;
		if(!dAction.getNpc().equals(npc)) return false;
		if(!UtilMethods.inventoryContainsItems(dAction.getInventory(), items)) return false;
		
		return true;
	}
	
	
	@Override
	public void actionCounted(Action action) {
					
		DeliverAction dAction = (DeliverAction) action;
		
		UtilMethods.removeItemsFromInventory(dAction.getInventory(), items);
	}

	
	private String itemsToString(ItemStack[] items) {
		
		String result = "";
		
		for(int i = 0; i < items.length; i++) {
			
			if(i == items.length) {
				result += " & ";
				
			} else {
				result += ", ";
			}
			
			ItemStack item = items[i];
			
			String itemName;
			if( item.hasItemMeta() && item.getItemMeta().hasDisplayName() ) {
				
				itemName = item.getItemMeta().getDisplayName();
			} else {
				
				itemName = WordUtils.capitalizeFully(item.getType().toString().replaceAll("_", " "));
			}
			
			result += item.getAmount() + " " + itemName;
		}
		
		return result;
	}
}
