package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Harvest;

import java.util.ArrayList;
import java.util.List;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * A task where a player has to harvest a specific type of plant or any type at all.
 * 
 * @author lJuanGB
 *
 */
public class HarvestTask extends APITask{

	public static final String path = "generic.task.harvest.";
	
	public static final String numberKey = "number";
	public static final String plantKey = "plantName";
	
	public static final String onePlant = "onePlant";
	public static final String multiplePlants = "multiplePlants";
	public static final String oneSpecificPlant = "oneSpecificPlant";
	public static final String multipleSpecificPlants = "multipleSpecificPlants";
	
	
	
	protected final Harvestable plant;
	
	
	public HarvestTask(String forcedDescription, int actionGoal, Harvestable plant) {
		super(forcedDescription, actionGoal);
	
		this.plant = plant;
	}
	
	public HarvestTask(int actionGoal, Harvestable plant) {
		this(null, actionGoal, plant);
	}
	
	public HarvestTask(String forcedDescription, int actionGoal) {
		this(forcedDescription, actionGoal, null);
	}
	
	public HarvestTask(int actionGoal) {
		this(null, actionGoal, null);
	}

	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(plant == null) {
			
			if(super.getActionGoal() == 1) {
				
				return path + onePlant;
				
			} else {
				
				return path + multiplePlants;
			}
			
		} else {
			
			if(super.getActionGoal() == 1) {
				
				return path + oneSpecificPlant;
				
			} else {
				
				return path + multipleSpecificPlants;
			}
		}

	}

	
	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		String plantName;
		
		if(plant == null) {
			
			plantName = "";
		} else {
			
			if(super.getActionGoal() == 1) {
				
				plantName = plant.getSingular();
				
			} else {
				
				plantName = plant.getPlural();
			}
		}
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(numberKey, super.getActionGoal() + ""));
		variables.add(new LanguageVariable(plantKey, plantName));
		
		return variables;
	}

	@Override
	public boolean shouldActionCount(Action action) {
		if(!(action instanceof HarvestAction)) return false;
		
		HarvestAction hAction = (HarvestAction) action;
		
		if(plant == null) return true;
		
		return plant.isMaterialValid(hAction.getMaterial());
	}

	
	public Harvestable getPlant() {
		return plant;
	}
	
}
