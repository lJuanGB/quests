package com.gmail.lJuanGBMinecraft.Task;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * A task that may only accept actions in a specific region
 * 
 * @author lJuanGB
 *
 */
public interface LocalizedTask{
	
	public boolean isInRange(Vector position, String world);
	
	public boolean isInRange(Location loc);

	public Region getRegion();
}