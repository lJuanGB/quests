package com.gmail.lJuanGBMinecraft.Task;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 * An action that may count towards a task completion
 * 
 * @author lJuanGB
 *
 */
public abstract class Action {

	private final Player player;
	
	private final String world;
	private final Vector position;
	private final ItemStack itemUsed;
	
	/**
	 * @param player The player who did the task
	 */
	public Action(Player player) {
		
		this.player = player;
		
		this.position = player.getLocation().toVector();
		this.world = player.getLocation().getWorld().getName();
		
		ItemStack itemInHand = player.getInventory().getItemInMainHand();
		
		if(itemInHand == null || itemInHand.getType().equals(Material.AIR)) {
			this.itemUsed =  null;
		} else {
			this.itemUsed =  itemInHand.clone();
		}
		
	}

	
	public Player getPlayer() {
		return player;
	}
	
	public Vector getPosition() {
		return position;
	}
	
	public ItemStack getItemUsed() {
		return itemUsed;
	}
	
	public String getWorld() {
		return world;
	}
	
}
