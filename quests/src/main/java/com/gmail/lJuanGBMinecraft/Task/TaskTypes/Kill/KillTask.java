package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Kill;

import java.util.List;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.google.common.collect.Lists;

/**
 * A task where a player has to kill a set amount of monsters.
 * May limit to named monsters.
 * 
 * @author lJuanGB
 */
public class KillTask extends APITask{
	
	public static final String monTypeKey = "monsterType";
	public static final String monNameKey = "monsterName";
	
	public static final String path = "generic.task.kill.";
	public static final String oneMonster = "oneMonster";
	public static final String multipleMonsters = "multipleMonsters";
	public static final String oneNamedMonster = "oneNamedMonster";
	public static final String multipleNamedMonsters = "multipleNamedMonsters";

	
	protected final MobType monType;
	protected final String monName;
	
	
	public KillTask(String forcedDescription, int actionGoal, MobType monType, String monName) {
		super(forcedDescription, actionGoal);
		
		this.monType = monType;
		this.monName = monName;
	}
	
	
	public KillTask(int actionGoal, MobType monType, String monName) {
		this(null, actionGoal, monType, monName);
	}
	
	public KillTask(String forcedDescription, int actionGoal, MobType monType) {
		this(forcedDescription, actionGoal, monType, null);
	}
	
	public KillTask(int actionGoal, MobType monType) {
		this(null, actionGoal, monType, null);
	}


	@Override
	public String getGeneratedDescriptionPath() {	
		
		if(this.getActionGoal() == 1 && monName != null) {
			
			return path + oneNamedMonster;
		} 
		
		if(this.getActionGoal() > 1 && monName != null) {
			
			return path + multipleNamedMonsters;
		} 
		
		if(this.getActionGoal() == 1 && monName == null) {
			
			return path + oneMonster;
		} 
		
		if(this.getActionGoal() > 1 && monName == null) {
			
			return path + multipleMonsters;
		} 
		
		return null;
	}
	
	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		LanguageVariable[] variables = new LanguageVariable[] {
				new LanguageVariable(monTypeKey, monType.getDisplayName(locale)),
				new LanguageVariable(monNameKey, monName),
		};
		
		return Lists.newArrayList(variables);
	}


	@Override
	public boolean shouldActionCount(Action action) {
		
		if(!(action instanceof KillAction)) return false;
		
		KillAction kAction = (KillAction) action;
		
		if(!kAction.getMonType().equals(monType)) return false;
		
		if(monName == null) return true;
		if(kAction.getMonsterName() == null) return false;
		
		return kAction.getMonsterName().equals(monName);
	}
	
	
	

}
