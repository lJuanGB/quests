package com.gmail.lJuanGBMinecraft.Task.TaskTypes.DigGrave;

import java.util.ArrayList;
import java.util.List;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * A task where a player has to dig graves.
 * 
 * @author lJuanGB
 *
 */
public class DigGraveTask extends APITask{

	public static final String path = "generic.task.dig.";
	
	public static final String numberKey = "number";
	
	public static final String oneGrave = "oneGrave";
	public static final String multipleGraves = "multipleGraves";

	
	public DigGraveTask(String forcedDescription, int actionGoal) {
		super(forcedDescription, actionGoal);
	}
	
	public DigGraveTask( int actionGoal) {
		this(null, actionGoal);
	}

	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(super.getActionGoal() == 1) {
			
			return path + oneGrave;
			
		} else {
			
			return path + multipleGraves;
		}
	}

	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(numberKey, super.getActionGoal() + ""));
		
		return variables;
	}

	@Override
	public boolean shouldActionCount(Action action) {
		if(action instanceof DigGraveAction) return true;
		
		return false;
	}

	
}
