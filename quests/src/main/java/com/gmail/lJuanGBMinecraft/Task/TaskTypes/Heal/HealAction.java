package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Heal;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * Action given to player that heal other players.
 * 
 * @author lJuanGB
 *
 */
public class HealAction extends Action{

	private final boolean usedRedDye;
	private final boolean usedGreenDye;
	
	
	public HealAction(Player player, boolean usedRedDye, boolean usedGreenDye) {
		super(player);
		this.usedRedDye = usedRedDye;
		this.usedGreenDye = usedGreenDye;
	}


	
	public boolean isRedDyeUsed() {
		return usedRedDye;
	}

	public boolean isGreenDyeUsed() {
		return usedGreenDye;
	}
	
	
}
