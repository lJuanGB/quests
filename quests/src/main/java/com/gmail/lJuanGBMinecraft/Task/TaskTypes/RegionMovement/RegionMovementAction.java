package com.gmail.lJuanGBMinecraft.Task.TaskTypes.RegionMovement;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * Action given to players who join or leave a region.
 * 
 * @author lJuanGB
 *
 */
public class RegionMovementAction extends Action{
	
	public enum MovementType{
		JOIN,
		LEAVE,
	}
	
	
	private final Region region;
	private final MovementType movementType;
	
	public RegionMovementAction(Player player, Region region, MovementType movement) {
		super(player);
		
		this.region = region;
		this.movementType = movement;
	}

	
	public Region getRegion() {
		return region;
	}

	public MovementType getMovementType() {
		return movementType;
	}

}
