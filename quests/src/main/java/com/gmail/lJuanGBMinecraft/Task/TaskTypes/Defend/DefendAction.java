package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Defend;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * Action given when a player successfully completes a DefenseInstance.
 * 
 * @author lJuanGB
 *
 */
public class DefendAction extends Action{

	private final int timeInSeconds;
	private final int maxMobCount;
	private final Region region;
	
	
	public DefendAction(Player player, int timeInSeconds, int maxMobCount,Region region) {
		
		super(player);
		this.timeInSeconds = timeInSeconds;
		this.maxMobCount = maxMobCount;
		this.region = region;
	}
	



	public int getTimeInSeconds() {
		return timeInSeconds;
	}


	public int getMaxMobCount() {
		return maxMobCount;
	}


	public Region getRegion() {
		return region;
	}
	
	
	
}
