package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Defend;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.LocalizedTask;
import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * A task where someone has to defend a region for an amount of time without letting
 * the mob count go over a limit.
 * 
 * @author lJuanGB
 *
 */
public class DefendTask extends APITask implements LocalizedTask{

	public static final String mobCountKey = "mobCount";
	public static final String regionNameKey = "regionName";
	public static final String timeKey = "time";

	public static final String path = "generic.task.defend.";
	public static final String description = "description";

	
	private final int timeInSeconds;
	private final int maxMobCount;
	private final Region region;
	

	public DefendTask(String forcedDescription, Region region,  int maxMobCount, int timeInSeconds) {
		super(forcedDescription, 1);
		
		this.maxMobCount = maxMobCount;
		this.timeInSeconds = timeInSeconds;
		
		this.region = region;
	}
	
	
	public DefendTask(Region region, int maxMobCount, int timeInSeconds) {
		this(null, region, maxMobCount, timeInSeconds);
	}

	
	
	public Region getRegion() {
		return this.region;
	}
	
	
	@Override
	public String getGeneratedDescriptionPath() {	
		
		return path + description;
	}
	
	
	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(mobCountKey, "" + maxMobCount));
		variables.add(new LanguageVariable(regionNameKey, region.getRegionName()));
		variables.add(new LanguageVariable(timeKey, UtilMethods.divideSeconds(timeInSeconds, locale)));
		
		return variables;
	}


	
	@Override
	public boolean shouldActionCount(Action action) {
		
		if(!(action instanceof DefendAction)) return false;
		
		DefendAction dAction = (DefendAction) action;
		
		if(!region.equals( dAction.getRegion() ) ) return false;
				
		if(maxMobCount < dAction.getMaxMobCount()) return false;
		
		if(timeInSeconds > dAction.getTimeInSeconds()) return false;
		
		return true;
	}




	public int getTimeInSeconds() {
		return timeInSeconds;
	}

	public int getMaxMobCount() {
		return maxMobCount;
	}


	@Override
	public boolean isInRange(Vector position, String world) {
		return region.isInside(position, world);
	}
	
	@Override
	public boolean isInRange(Location loc) {
		return region.isInside(loc);
	}
}
