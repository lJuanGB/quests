package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Kill;


import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;

/**
 * Represents the basic current mob types that are in MineZ
 * 
 * Probably changed to internal MineZ mob identification system for consistency
 * if ever added to the game.
 * 
 * @author lJuanGB
 */
public enum MobType {

	ZOMBIE("zombie", EntityType.ZOMBIE),
	PIGMAN("pigman", EntityType.PIG_ZOMBIE),
	GIANT("giant", EntityType.GIANT),
	BABY_ZOMBIE("baby_zombie", EntityType.ZOMBIE, true),
	BABY_PIGMAN("baby_pigman", EntityType.PIG_ZOMBIE, true),
	SKELETON("skeleton", EntityType.SKELETON),
	
	PLAYER("player", EntityType.PLAYER),
	CHICKEN("chicken", EntityType.CHICKEN);
	
	
	public static final String path = "generic.task.kill.mobName.";
	
	private final String namePath;
	private final EntityType type;
	private final boolean isBaby;	
	private final boolean isMonster;
	
	/**
	 * 
	 * @param namePath Path of the name used when refering to this mob
	 * @param isMonster Default true. If its a hostile monster.
	 * @param type Bukkit EntityType of the mob
	 * @param isBaby Default false.
	 */
	MobType(String namePath, boolean isMonster, EntityType type, boolean isBaby) {
		this.namePath = namePath;
		this.type = type;
		this.isBaby = isBaby;
		this.isMonster = isMonster;
	}
	
	MobType(String namePath, boolean isMonster, EntityType type){
		this(namePath, isMonster, type, false);
	}
	
	MobType(String namePath, EntityType type, boolean isBaby) {
		this(namePath, true, type, isBaby);
	}
	
	MobType(String namePath, EntityType type){
		this(namePath, true, type, false);
	}
	
	
	public String getNamePath() {
		return namePath;
	}

	public EntityType getType() {
		return type;
	}

	public boolean isBaby() {
		return isBaby;
	}
	
	public boolean isMonster() {
		return isMonster;
	}
	
	public static MobType getType(Entity ent) {
		
		for(MobType type : MobType.values()) {
							
			if(!ent.getType().equals(type.getType())) continue;
			
			if(type.isBaby() && ent instanceof Zombie) {
				if( !((Zombie) ent).isBaby() ) continue;
			}

			return type;
		}
		
		return null;
	}
	
	
	public String getDisplayName(String locale) {
		return QuestsAPIPlugin.api.getMessage(locale, path + this.getNamePath());
	}

}
