package com.gmail.lJuanGBMinecraft.Task;

import org.bukkit.plugin.Plugin;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;

/**
 * Represents a task declared by the API plugin
 * 
 * The only difference between Task is that the getDeclaringPlugin() method
 * is implemented and returns the QuestsPlugin instance
 * 
 * @author lJuanGB
 *
 */
public abstract class APITask extends Task{

	public APITask(String forcedDescription, int actionGoal) {
		super(forcedDescription, actionGoal);
	}
	
	public  APITask(int actionGoal) {
		this(null, actionGoal);
	}

	@Override
	public Plugin getDeclaratorPlugin(){
		return QuestsAPIPlugin.getInstance();
	}
	
}
