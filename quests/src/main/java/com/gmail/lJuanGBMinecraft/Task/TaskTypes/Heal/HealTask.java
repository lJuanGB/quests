package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Heal;

import java.util.ArrayList;
import java.util.List;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * A task where a player has to heal a number of players.
 * You may specify if these heals must be with the dyes or not.
 * 
 * @author lJuanGB
 *
 */
public class HealTask extends APITask{

	public static final String path = "generic.task.heal.";
	
	public static final String numberKey = "number";
	
	public static final String oneHeal = "oneHeal";
	public static final String multipleHeals = "multipleHeals";
	public static final String oneHealWithRed = "oneHealWithRed";
	public static final String multipleHealsWithRed = "multipleHealsWithRed";
	public static final String oneHealWithGreen = "oneHealWithGreen";
	public static final String multipleHealsWithGreen = "multipleHealsWithGreen";
	public static final String oneHealWithBoth = "oneHealWithBoth";
	public static final String multipleHealsWithBoth = "multipleHealsWithBoth";
	
	
	private final boolean withRedDye;
	private final boolean withGreenDye;
	
	public HealTask(String forcedDescription, int actionGoal, boolean withRedDye, boolean withGreenDye) {
		super(forcedDescription, actionGoal);
		
		this.withRedDye = withRedDye;
		this.withGreenDye = withGreenDye;
	}
	
	public HealTask(int actionGoal, boolean withRedDye, boolean withGreenDye) {
		this(null, actionGoal, withRedDye, withGreenDye);
	}

	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(super.getActionGoal() == 1) {
			
			if(isWithRedDye() && isWithGreenDye()) return path + oneHealWithBoth;
				
			if (isWithRedDye()) return path + oneHealWithRed;
				
			if (isWithGreenDye()) return path + oneHealWithGreen;
				
			return path + oneHeal;
			
		} else {
			
			if(isWithRedDye() && isWithGreenDye()) return path + multipleHealsWithBoth;
			
			if (isWithRedDye()) return path + multipleHealsWithRed;
				
			if (isWithGreenDye()) return path + multipleHealsWithGreen;
				
			return path + multipleHeals;
		}
	}

	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(numberKey, super.getActionGoal() + ""));
		
		return variables;
	}

	@Override
	public boolean shouldActionCount(Action action) {
		if(!(action instanceof HealAction)) return true;
		
		HealAction hAction = (HealAction) action;
		
		if(withRedDye && !hAction.isRedDyeUsed()) return false;
		if(withGreenDye && !hAction.isGreenDyeUsed()) return false;
		
		return false;
	}


	public boolean isWithRedDye() {
		return withRedDye;
	}

	public boolean isWithGreenDye() {
		return withGreenDye;
	}


}