package com.gmail.lJuanGBMinecraft.Task.TaskTypes.DigGrave;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.LocalizedTask;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * A task where a player has to dig graves in a specific region
 * 
 * @author lJuanGB
 *
 */
public class LocalizedDigGraveTask extends DigGraveTask implements LocalizedTask{

	public static final String path = "generic.task.dig.localized.";
	
	public static final String regionNameKey = "regionName";
	
	private final Region region;
	
	public LocalizedDigGraveTask(String forcedDescription, int actionGoal, Region region) {
		super(forcedDescription, actionGoal);
		
		this.region = region;
	}

	
	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(super.getActionGoal() == 1) {
			
			return path + oneGrave;
			
		} else {
			
			return path + multipleGraves;
		}
	}
	

	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = super.getGeneratedVariables(locale);
		variables.add(new LanguageVariable(regionNameKey, region.getRegionName()));

		return variables;
	}

	@Override
	public boolean shouldActionCount(Action action) {
		
		if( !isInRange(action.getPosition(), action.getWorld()) ) return false;
				
		return super.shouldActionCount(action);
	}


	@Override
	public boolean isInRange(Vector position, String world) {
		return region.isInside(position, world);
	}



	@Override
	public Region getRegion() {
		return region;
	}
	
	@Override
	public boolean isInRange(Location loc) {
		return region.isInside(loc);
	}
}
