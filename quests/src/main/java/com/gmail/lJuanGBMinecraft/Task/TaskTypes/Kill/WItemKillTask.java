package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Kill;

import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.WItemTask;
import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;

/**
 * A task where a player has to kill a set amount of monsters with a specific item. 
 * This uses a soft item equality check. See UtilMethods.areItemsSimilar()
 * May limit to named monsters.
 * 
 * @author lJuanGB
 */
public class WItemKillTask extends KillTask implements WItemTask{
	
	public static final String itemNameKey = "itemName";
	
	public static final String path = "generic.task.kill.withItem";
	
	
	private final ItemStack item;
	
	
	public WItemKillTask(int actionGoal, MobType monType, String monName, ItemStack item) {
		super(actionGoal, monType, monName);
		
		this.item = item;
	}
	
	public WItemKillTask(int actionGoal, MobType monType, ItemStack item) {
		super(actionGoal, monType);
		
		this.item = item;
	}
	
	public WItemKillTask(String forcedDescription, int actionGoal, MobType monType, String monName, ItemStack item) {
		super(forcedDescription, actionGoal, monType, monName);
		
		this.item = item;
	}
	
	public WItemKillTask(String forcedDescription, int actionGoal, MobType monType, ItemStack item) {
		super(forcedDescription, actionGoal, monType);
		
		this.item = item;
	}
	
	
	
	@Override
	public boolean isItemValid(ItemStack item) {
		
		if(!UtilMethods.areItemsSimilar(this.item, item)) return false;
		
		return true;
	}
	
	
	
	@Override
	public String getGeneratedDescriptionPath() {	
		
		if(this.getActionGoal() == 1 && monName != null) {
			
			return path + oneNamedMonster;
		} 
		
		if(this.getActionGoal() > 1 && monName != null) {
			
			return path + multipleNamedMonsters;
		} 
		
		if(this.getActionGoal() == 1 && monName == null) {
			
			return path + oneMonster;
		} 
		
		if(this.getActionGoal() > 1 && monName == null) {
			
			return path + multipleMonsters;
		} 
		
		return null;
	}
	
	
	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
				
		List<LanguageVariable> variables = super.getGeneratedVariables(locale);
		variables.add(new LanguageVariable(itemNameKey, UtilMethods.itemToString(item)));
		
		return variables;
	}


	
	@Override
	public boolean shouldActionCount(Action action) {
		
		if(!isItemValid(action.getItemUsed())) return false;
		
		return super.shouldActionCount(action);
	}
}
