package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Deliver;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.gmail.lJuanGBMinecraft.DialogerAPI.NPC.DialogerNPC;
import com.gmail.lJuanGBMinecraft.NPC.CompleteDeliverTaskDialog;
import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * Action given when a player talks with an NPC.
 * 
 * This action stores the player inventory and may 
 * 
 * @author lJuanGB
 *
 */
public class DeliverAction extends Action{

	private final DialogerNPC npc;
	private final CompleteDeliverTaskDialog dialog;
	private final Inventory inv;
	
	public DeliverAction(Player player, CompleteDeliverTaskDialog dialog, DialogerNPC npc) {
		super(player);
		
		this.npc = npc;
		this.dialog = dialog;
		this.inv = player.getInventory();
	}

	
	public CompleteDeliverTaskDialog getDialog() {
		return dialog;
	}

	public DialogerNPC getNpc() {
		return npc;
	}
	
	public Inventory getInventory() {
		return inv;
	}

}
