package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Talk;

import java.util.ArrayList;
import java.util.List;

import com.gmail.lJuanGBMinecraft.DialogerAPI.NPC.DialogerNPC;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.NPC.CompleteTalkTaskDialog;
import com.gmail.lJuanGBMinecraft.Task.APITask;
import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * A task where a player has to reach a specific dialog with certain NPC.
 * 
 * @author lJuanGB
 *
 */
public class TalkTask extends APITask{

	public static final String path = "generic.task.talk.";
	
	public static final String nameNPCKey = "nameNPC";
	
	public static final String talk = "talk";
	
	
	private final CompleteTalkTaskDialog dialog;
	private final DialogerNPC npc;

	
	public TalkTask(String forcedDescription, CompleteTalkTaskDialog dialog, DialogerNPC npc) {
		super(forcedDescription, 1);
		
		this.dialog = dialog;
		this.npc = npc;
	}
	
	public TalkTask(CompleteTalkTaskDialog dialog, DialogerNPC npc) {
		this(null, dialog, npc);
	}


	
	public CompleteTalkTaskDialog getDialog() {
		return dialog;
	}

	public DialogerNPC getNpc() {
		return npc;
	}

	
	
	@Override
	public String getGeneratedDescriptionPath() {
		return path + talk;
	}


	
	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(nameNPCKey, npc.getName()));
		
		return variables;
	}




	@Override
	public boolean shouldActionCount(Action action) {
		
		if(!(action instanceof TalkAction)) return false;
		
		TalkAction dAction = (TalkAction) action;
		
		if(!dAction.getDialog().isSimilar(dialog)) return false;
		if(!dAction.getNpc().equals(npc)) return false;
		
		return true;
	}

}
