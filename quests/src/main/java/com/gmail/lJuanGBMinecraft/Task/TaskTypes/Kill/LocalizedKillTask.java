package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Kill;


import java.util.List;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.LocalizedTask;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * A task where a player has to kill a set amount of monsters in a region.
 * May limit to named monsters.
 * 
 * @author lJuanGB
 */
public class LocalizedKillTask extends KillTask implements LocalizedTask{

	public static final String regionNameKey = "regionName";
	
	public static final String path = "generic.task.kill.localized";
	
	private final Region region;
	
	
	public LocalizedKillTask(String forcedDescription, int actionGoal, MobType monType, String monName, Region region) {
		super(forcedDescription, actionGoal, monType, monName);

		this.region = region;
	}
	
	public LocalizedKillTask(int actionGoal, MobType monType, String monName, Region region) {
		this(null, actionGoal, monType, monName, region);
	}
	
	public LocalizedKillTask(String forcedDescription, int actionGoal, MobType monType, Region region) {
		this(forcedDescription, actionGoal, monType, null, region);
	}
	
	public LocalizedKillTask(int actionGoal, MobType monType, Region region) {
		this(null, actionGoal, monType, null, region);
	}
	
	
	
	@Override
	public boolean isInRange(Vector pos, String world) {
		
		return region.isInside(pos, world);
	}

	
	@Override
	public Region getRegion() {
		return this.region;
	}
	
	
	@Override
	public String getGeneratedDescriptionPath() {	
		
		if(this.getActionGoal() == 1 && monName != null) {
			
			return path + oneNamedMonster;
		} 
		
		if(this.getActionGoal() > 1 && monName != null) {
			
			return path + multipleNamedMonsters;
		} 
		
		if(this.getActionGoal() == 1 && monName == null) {
			
			return path + oneMonster;
		} 
		
		if(this.getActionGoal() > 1 && monName == null) {
			
			return path + multipleMonsters;
		} 
		
		return null;
	}
	
	
	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = super.getGeneratedVariables(locale);
		variables.add(new LanguageVariable(regionNameKey, region.getRegionName()));
		
		return variables;
	}


	
	@Override
	public boolean shouldActionCount(Action action) {
		
		if(!isInRange(action.getPosition(), action.getWorld())) return false;
		
		return super.shouldActionCount(action);
	}
	
	@Override
	public boolean isInRange(Location loc) {
		return region.isInside(loc);
	}
	
}
