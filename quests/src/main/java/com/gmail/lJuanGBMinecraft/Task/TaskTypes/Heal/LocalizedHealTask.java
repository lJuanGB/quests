package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Heal;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.LocalizedTask;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;
/**
 * A task where a player has to heal a number of players in a specific region.
 * You may specify if these heals must be with the dyes or not.
 * 
 * @author lJuanGB
 *
 */
public class LocalizedHealTask extends HealTask implements LocalizedTask{

	public static final String path = "generic.task.heal.localized.";
	
	public static final String regionNameKey = "regionName";
	
	private final Region region;
	
	public LocalizedHealTask(String forcedDescription, int actionGoal, boolean withRedDye, boolean withGreenDye, Region region) {
		super(forcedDescription, actionGoal, withRedDye, withGreenDye);
		
		this.region = region;
	}

	
	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(super.getActionGoal() == 1) {
			
			if(isWithRedDye() && isWithGreenDye()) return path + oneHealWithBoth;
				
			if (isWithRedDye()) return path + oneHealWithRed;
				
			if (isWithGreenDye()) return path + oneHealWithGreen;
				
			return path + oneHeal;
			
		} else {
			
			if(isWithRedDye() && isWithGreenDye()) return path + multipleHealsWithBoth;
			
			if (isWithRedDye()) return path + multipleHealsWithRed;
				
			if (isWithGreenDye()) return path + multipleHealsWithGreen;
				
			return path + multipleHeals;
		}
	}
	

	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = super.getGeneratedVariables(locale);
		variables.add(new LanguageVariable(regionNameKey, region.getRegionName()));

		return variables;
	}

	@Override
	public boolean shouldActionCount(Action action) {
		
		if( !isInRange(action.getPosition(), action.getWorld()) ) return false;
				
		return super.shouldActionCount(action);
	}


	@Override
	public boolean isInRange(Vector position, String world) {
		return region.isInside(position, world);
	}



	@Override
	public Region getRegion() {
		return region;
	}
	
	@Override
	public boolean isInRange(Location loc) {
		return region.isInside(loc);
	}
}
