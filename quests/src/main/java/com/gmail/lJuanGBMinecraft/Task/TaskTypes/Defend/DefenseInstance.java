package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Defend;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.Kill.MobType;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;
import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * Handles checking if defense of a region is successful or not.
 * 
 * This creates a boss bar for the player to see the progress.
 * 
 * @author lJuanGB
 *
 */
public class DefenseInstance extends BukkitRunnable{

	public final static String path = "generic.task.defend.defenseInstance.";
	
	public final static String initial = "initial";
	public final static String bossBar = "bossBar";
	
	public final static String succeedTitle = "successfulTitle";
	public final static String succeedSubtitle = "successfulSubtitle";
	
	public final static String fail = "fail";
	public final static String playerLeave = "playerLeave";
	public final static String tooManyMobs = "tooManyMobs";
	
	public final static String timeKey = "timeLeft";
	public final static String mobsKey = "mobsInside";
	public final static String maxMobsKey = "maxMobs";
	public static final String regionNameKey = "regionName";
	

	public final static int timeUntilStart = 100;
	
	private final Player player;
	
	private final int timeInSeconds;
	private final int maxMobCount;
	private final Region region;
	
	private BossBar bar;
	
	private int timePassed = 0;
	
	private boolean started = false;
	private boolean finished = false;
	
	
	public DefenseInstance(Player player, int timeInSeconds, int maxMobCount, Region region) {

		this.player = player;
		this.timeInSeconds = timeInSeconds;
		this.maxMobCount = maxMobCount;
		this.region = region;
		
		this.bar = Bukkit.createBossBar(QuestsAPIPlugin.api.getMessage(player, path + initial), BarColor.GREEN, BarStyle.SEGMENTED_10);
		bar.setVisible(true);

	}
	
	public DefenseInstance(Player player, DefendTask task) {
		this(player, task.getTimeInSeconds(), task.getMaxMobCount(), task.getRegion());
	}

	
	
	public boolean start() {
		
		if(this.hasStarted()) return false;
		if(DefenseMap.contains(getPlayer())) return false;
		
		this.runTaskTimer(QuestsAPIPlugin.getInstance(), timeUntilStart, 20);
		DefenseMap.register(this);
		this.started = true;
		return true;
	}
	
	public boolean hasStarted() {
		return this.started;
	}

	private static final double blinkPercentage = 0.1;
	private static final double yellowPercentage = 0.3;

	@Override
	public void run() {
		
		if(!player.isOnline()) this.fail(null);
		
		if(!region.isInside(player.getLocation())) this.fail(path + playerLeave);
		
		int mobsInside = mobsInside();
		if(mobsInside > maxMobCount) this.fail(path + tooManyMobs);
		
		
		
		timePassed++;
		
		
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(timeKey, UtilMethods.formatSecondsToClock(timePassed)));
		variables.add(new LanguageVariable(mobsKey, "" + mobsInside));
		variables.add(new LanguageVariable(maxMobsKey, "" + maxMobCount));
		variables.add(new LanguageVariable(regionNameKey, region.getRegionName()));
		
		bar.setTitle( QuestsAPIPlugin.api.getMessage(player, path + bossBar, variables) );

		
		
		double percentage = (0.0 + timePassed)/( 0.0 + timeInSeconds);
		bar.setProgress( percentage );
		
		if(percentage > blinkPercentage && percentage <= yellowPercentage) {
			bar.setColor(BarColor.YELLOW);
		}
		
		if(percentage <= blinkPercentage) {
			if(timePassed % 2 == 0) bar.setColor(BarColor.RED);
			if(timePassed % 2 == 1) bar.setColor(BarColor.YELLOW);
		}
		
		
		
		if(timePassed >= timeInSeconds) succeed();
	}
	
	
	
	
	private static final int delayFail = 100;

	/**
	 * Forces the defense to fail
	 * 
	 * @param messagePath Should be either playerLeave or tooManyMobs
	 * @return false if the defense had already finished
	 */
	@SuppressWarnings("deprecation")
	public boolean fail(String messagePath) {
		
		if(finished) return false;
		
		deleteBar(delayFail);
		
		player.sendTitle(path + fail, path + messagePath);
		return true;
	}
	
	
	private static final int delaySucceed = 100;
	
	/**
	 * Force the defense to succeed
	 * 
	 * @return false if the defense had already finished
	 */
	@SuppressWarnings("deprecation")
	public boolean succeed() {
		
		if(finished) return false;
		
		deleteBar(delaySucceed);
		
		player.sendTitle(path + succeedTitle, path + succeedSubtitle);
		DefendAction action = new DefendAction(player, timeInSeconds, maxMobCount, region);
		
		PlayerQuestHelper.sendAction(player, action);
		return true;
	}
	
	
	
	public Player getPlayer() {
		return player;
	}

	
	private void deleteBar(long delay) {
		
		BukkitRunnable runnable = new BukkitRunnable() {
			
		  @Override
          public void run() {
        	  deleteBar();
          }     
        };
        
        runnable.runTaskLater(QuestsAPIPlugin.getInstance(), delay);
	}
	
	private void deleteBar() {
		bar.removeAll();
		bar.setVisible(false);
	}
	
	
	
	private int mobsInside() {
		
		int count = 0;
		List<Entity> ents = Bukkit.getWorld(region.getWorld()).getEntities();
		
		for(Entity ent : ents) {
			
			if(MobType.getType(ent) != null && MobType.getType(ent).isMonster()) count++;
		}
		
		return count;
	}
	
	
}
