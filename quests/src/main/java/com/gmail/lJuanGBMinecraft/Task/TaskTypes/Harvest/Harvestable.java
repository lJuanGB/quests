package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Harvest;

import org.bukkit.Material;

/**
 * Represents items currently harvestable with a how in MineZ
 * 
 * @author lJuanGB
 *
 */
public enum Harvestable {

	MELON( "Melon", "Melons", Material.MELON),
	WHEAT( "Wheat Crop", "Wheat Crops", Material.WHEAT),
	RED_MUSHROOM("Red Mushroom", "Red Mushrooms", Material.RED_MUSHROOM),
	BROWN_MUSHROOM("Brown Mushroom", "Brown Mushrooms", Material.BROWN_MUSHROOM),
	MUSHROOM("Mushroom", "Mushrooms", Material.BROWN_MUSHROOM, Material.RED_MUSHROOM),
	;
	
	
	private final String singular;
	private final String plural;
	private final Material[] validMaterials;
	
	
	Harvestable(String singular, String plural, Material... validMaterials){
		
		this.singular = singular;
		this.plural = plural;
		this.validMaterials = validMaterials;
	}


	public String getSingular() {
		return singular;
	}


	public String getPlural() {
		return plural;
	}


	public Material[] getValidMaterials() {
		return validMaterials;
	}
	
	
	public boolean isMaterialValid(Material material) {
		
		for(Material m : validMaterials) {
			
			if(m.equals(material)) return true;
		}
		return false;
	}
}
