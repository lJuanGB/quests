package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Harvest;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * Action given when a player harvests something.
 * 
 * @author lJuanGB
 *
 */
public class HarvestAction extends Action{

	private final Material material;
	
	public HarvestAction(Player player, Material material) {
		super(player);
		
		this.material = material;
	}

	
	public Material getMaterial() {
		
		return material;
	}
}
