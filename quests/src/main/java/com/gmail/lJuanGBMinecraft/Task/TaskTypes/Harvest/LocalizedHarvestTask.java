package com.gmail.lJuanGBMinecraft.Task.TaskTypes.Harvest;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.LocalizedTask;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

/**
 * A task where a player has to harvest a specific type of plant or any plant at all
 * in a specific region.
 * 
 * @author lJuanGB
 *
 */
public class LocalizedHarvestTask extends HarvestTask implements LocalizedTask{

	public static final String path = "generic.task.loot.localized.";
	public static final String regionNameKey = "regionName";
	
	
	
	private final Region region;
	

	public LocalizedHarvestTask(String forcedDescription, int actionGoal, Harvestable plant, Region region) {
		super(forcedDescription, actionGoal, plant);

		this.region = region;
	}
	
	public LocalizedHarvestTask(int actionGoal, Harvestable plant, Region region) {
		this(null, actionGoal, plant, region);
	}

	public LocalizedHarvestTask(int actionGoal, Region region) {
		this(null, actionGoal, null, region);
	}


	public LocalizedHarvestTask(String forcedDescription, int actionGoal, Region region) {
		this(forcedDescription, actionGoal, null, region);
	}

	
	
	
	
	@Override
	public String getGeneratedDescriptionPath() {
		
		if(plant == null) {
			
			if(super.getActionGoal() == 1) {
				
				return path + onePlant;
				
			} else {
				
				return path + multiplePlants;
			}
			
		} else {
			
			if(super.getActionGoal() == 1) {
				
				return path + oneSpecificPlant;
				
			} else {
				
				return path + multipleSpecificPlants;
			}
		}

	}
	

	@Override
	public List<LanguageVariable> getGeneratedVariables(String locale) {
		
		List<LanguageVariable> variables = super.getGeneratedVariables(locale);
		variables.add(new LanguageVariable(regionNameKey, region.getRegionName()));

		return variables;
	}

	
	
	@Override
	public boolean shouldActionCount(Action action) {
		
		if( !isInRange(action.getPosition(), action.getWorld()) ) return false;
				
		return super.shouldActionCount(action);
	}
	
	
	
	@Override
	public boolean isInRange(Vector position, String world) {
		return region.isInside(position, world);
	}

	@Override
	public Region getRegion() {
		return region;
	}
	
	@Override
	public boolean isInRange(Location loc) {
		return region.isInside(loc);
	}
}
