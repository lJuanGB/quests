package com.gmail.lJuanGBMinecraft.PlayerListeners;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class PlayerLeaveListener implements Listener{

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		
		onPlayerLeave(e.getPlayer());
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent e) {
		
		onPlayerLeave(e.getPlayer());
	}

	
	private void onPlayerLeave(Player player) {
		
		if(!player.hasPermission("quests.api.save")) return;
		
		List<String> accepted = serialize(PlayerQuestHelper.getAcceptedQuests(player));
		List<String> completed = toString(PlayerQuestHelper.getCompletedQuestsKeys(player));
		
		
		if(QuestsAPIPlugin.useMySql) {
			
			saveSql(player, accepted, completed);
		} else {
			
			saveFile(player, accepted, completed);
		}
	}


	private List<String> toString(List<?> listObjects) {

		List<String> list = new ArrayList<String>();
		
		for(Object obj : listObjects) {
			
			list.add(obj.toString());
		}
		
		return list;
	}

	private List<String> serialize(List<AcceptedQuest> acceptedQuests) {

		List<String> list = new ArrayList<String>();
		
		for(AcceptedQuest quest : acceptedQuests) {
			list.add(quest.serialize());
		}
		
		return list;
	}
	
	
	public static final String acceptedKey = "accepted";
	public static final String completedKey = "completed";
	
	private void saveFile(Player player, List<String> accepted, List<String> completed) {

		String uuidS = player.getUniqueId().toString();
		String path = QuestsAPIPlugin.getInstance().getDataFolder()+"/players/"+uuidS+".yml";
	    File file = new File(path);
		YamlConfiguration yamlFile = YamlConfiguration.loadConfiguration(file);
		
		yamlFile.set(acceptedKey, accepted);
		yamlFile.set(completedKey, completed);
	
		try {
			yamlFile.save(file);
		} catch (IOException e) {
			
			Bukkit.getLogger().log(Level.WARNING, 
					"Failed to save data for: " + player.getName());
			
			e.printStackTrace();
		}
	}

	private void saveSql(Player player, List<String> accepted, List<String> completed) {
		// TODO Implement
		
	}	
	
	
}
