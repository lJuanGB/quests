package com.gmail.lJuanGBMinecraft.PlayerListeners;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.LanguageAPI.MessagePosition;
import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Quest.NamespacedKey;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class PlayerJoinListener implements Listener{

	private final static String path = "announcements.quest.cancelled_reload";
	private final static String title = "title";
	private final static String subtitle = "subtitle";
	private final static String chat = "chat";
	
	private final static String questNameKey = "questName";

	
	private final Set<UUID> playerLoaded = new HashSet<UUID>();
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		
		Player player = e.getPlayer();
		
		if(!player.hasPermission("quests.api.load")) return;
		
		if(playerLoaded.contains(player.getUniqueId())) return;
		
		if(QuestsAPIPlugin.useMySql) {
			
			loadSql(player);
		
		} else {
			
			loadFile(player);
		}
		
		playerLoaded.add( player.getUniqueId() );
	}

	
	private void loadFile(Player player) {
		
		String uuidS = player.getUniqueId().toString();
		String path = QuestsAPIPlugin.getInstance().getDataFolder()+"/players/"+uuidS+".yml";
	    File file = new File(path);
		YamlConfiguration yamlFile = YamlConfiguration.loadConfiguration(file);
		
		List<String> serAccepted =yamlFile.getStringList(PlayerLeaveListener.acceptedKey);
		List<String> strCompleted = yamlFile.getStringList(PlayerLeaveListener.completedKey);
		
		loadFromLists(player, serAccepted, strCompleted);
	}

	private void loadSql(Player player) {
		// TODO Implement
		
	}
	
	private void loadFromLists(Player player, List<String> acceptedQuest, List<String> completedQuest) {
		
		List<AcceptedQuest> quests = deserialize(acceptedQuest);
		
		for(AcceptedQuest quest : quests.toArray(new AcceptedQuest[0])) {
			
			if(quest.getQuest().isDeleteAtReload() || quest.getQuest().isLegacy()) {
				
				List<LanguageVariable> var = new ArrayList<LanguageVariable>();
				String name = QuestsAPIPlugin.api.getMessage(player, quest.getQuest().getNamePath());
				var.add( new LanguageVariable(questNameKey, name));
				
				QuestsAPIPlugin.api.sendMessage(MessagePosition.TITLE, player, path + title, var);
				QuestsAPIPlugin.api.sendMessage(MessagePosition.SUBTITLE, player, path + subtitle, var);
				QuestsAPIPlugin.api.sendMessage(MessagePosition.CHAT, player, path + chat, var);

				quests.remove(quest);
			}
		}
		
		PlayerQuestHelper.setAcceptedQuests(player, quests);
		PlayerQuestHelper.setCompletedQuestsKeys(player, toNamespacedKeys(completedQuest));
	}
	
	
	private List<NamespacedKey> toNamespacedKeys(List<String> strCompleted) {
		
		List<NamespacedKey> keys = new ArrayList<NamespacedKey>();
		
		for(String s : strCompleted) {
			
			keys.add( new NamespacedKey(s) );
		}
		
		return keys;
	}


	private List<AcceptedQuest> deserialize(List<String> serAccepted) {
		
		List<AcceptedQuest> quests = new ArrayList<AcceptedQuest>();
		
		for(String s : serAccepted) {
			
			quests.add( AcceptedQuest.deserialize(s) );
		}
		
		return quests;
	}
}
