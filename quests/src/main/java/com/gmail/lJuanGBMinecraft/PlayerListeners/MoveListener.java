package com.gmail.lJuanGBMinecraft.PlayerListeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.Defend.DefendTask;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.RegionMovement.RegionMovementAction;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.RegionMovement.RegionMovementAction.MovementType;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.RegionMovement.RegionMovementTask;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;
import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class MoveListener implements Listener{

	public static final String path = "generic.task.defend.startCommand.";
	
	public static final String start = "start";
	
	public static final String questNameKey = "questName";
	
	
	public static final String prevRegionKey = "previouslyBeenRegion";
	
	@SuppressWarnings("unchecked")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent ev) {
		
		
		Vector vecFrom = ev.getFrom().toVector();
		Vector vecTo = ev.getTo().toVector();
		
		Vector vecDif = vecFrom.subtract(vecTo);
		
		if( 
				ev.getFrom().getWorld().equals( ev.getTo().getWorld() )
				
				&& Math.abs(vecDif.getX()) < 1
				&& Math.abs(vecDif.getY()) < 1
				&& Math.abs(vecDif.getZ()) < 1
		  
				) {
			
			return;
		}
		
		
		
		Player player = ev.getPlayer();
		
		//Get regions where the player last were
		List<Region> previousRegion = new ArrayList<Region>();
		
		if(UtilMethods.hasMetadataValue(player, prevRegionKey)) {
			
			previousRegion = (List<Region>) UtilMethods.getMetadataValue(player, prevRegionKey);
		}
		
		
		List<AcceptedQuest> aQuest = PlayerQuestHelper.getAcceptedQuests(player);
		
		//Get all regions where the player may join or leave from the player list of quests
		Map<Region, Quest> currentRegMov = PlayerQuestHelper.getRegions(aQuest, ev.getTo(), RegionMovementTask.class);
		
		
		//Send action for regions that are new
		List<Region> newRegions = new ArrayList<Region>(currentRegMov.keySet());
		newRegions.removeAll(previousRegion);
		
		for(Region reg : newRegions) {
			
			RegionMovementAction action = new RegionMovementAction(player, reg, MovementType.JOIN);
			PlayerQuestHelper.sendAction(player, action);
		}
		
		//Send actions for regions that are no longer
		List<Region> oldRegions = new ArrayList<Region>(previousRegion);
		oldRegions.removeAll(currentRegMov.keySet());
		
		for(Region reg : newRegions) {
			
			RegionMovementAction action = new RegionMovementAction(player, reg, MovementType.LEAVE);
			PlayerQuestHelper.sendAction(player, action);
		}
		
				
		//Get all regions where the player may have to defend and remove the ones that were already there last time
		//this event was called. Send a message if they want to start the defense,
		Map<Region, Quest> currentRegDef = PlayerQuestHelper.getRegions(aQuest, ev.getTo(), DefendTask.class);
		
		List<Region> newRegionsDef = new ArrayList<Region>(currentRegDef.keySet());
		newRegionsDef.removeAll(previousRegion);
		
		for(Region reg : newRegionsDef) {
			
			String questName = QuestsAPIPlugin.api.getMessage(player, currentRegDef.get(reg).getNamePath());
			sendDefenseMessage(player, questName);
		}
		
		
		List<Region> allCurrentRegions = new ArrayList<Region>(currentRegDef.keySet());
		allCurrentRegions.addAll(currentRegMov.keySet());
		
		UtilMethods.setMetadataValue(player, prevRegionKey, allCurrentRegions);
	}
	
	
	
	public static final String acceptSymbol = "[\u2714]";
	
	private void sendDefenseMessage(Player player, String questName) {
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		variables.add(new LanguageVariable(questNameKey, questName));
		
		String message = QuestsAPIPlugin.api.getMessage(player, path + start, variables);
		
		
		BaseComponent[] comp = TextComponent.fromLegacyText(message);
		
		TextComponent acceptSymbol = new TextComponent(MoveListener.acceptSymbol);
		acceptSymbol.setColor(ChatColor.GREEN);
		acceptSymbol.setBold(true);
		acceptSymbol.setClickEvent( new ClickEvent(Action.RUN_COMMAND, "/startdefense") );
		
		comp = (BaseComponent[]) ArrayUtils.add(comp, acceptSymbol);
		
		player.spigot().sendMessage(comp);
	}
}
