package com.gmail.lJuanGBMinecraft.NPC;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.DialogerAPI.NPC.Dialog;
import com.gmail.lJuanGBMinecraft.DialogerAPI.NPC.DialogerNPC;
import com.gmail.lJuanGBMinecraft.DialogerAPI.Requirements.Requirement;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.TaskTypes.Talk.TalkAction;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class CompleteTalkTaskDialog extends Dialog{
	
	
	public CompleteTalkTaskDialog(String trigger, String response, List<Requirement> reqShow, List<Requirement> reqClick) {
		super(trigger, response, reqShow, reqClick);
	}
	
	public CompleteTalkTaskDialog(String trigger, String response, List<Requirement> reqShow) {
		this(trigger, response, reqShow, new ArrayList<Requirement>());
	}
	
	public CompleteTalkTaskDialog(String trigger, String response) {
		this(trigger, response, new ArrayList<Requirement>(), new ArrayList<Requirement>());
	}
	
	public CompleteTalkTaskDialog(CompleteTalkTaskDialog dialog) {
		this(dialog.getRawTrigger(), dialog.getRawResponse(), new ArrayList<Requirement>(dialog.getReqShow()), new ArrayList<Requirement>(dialog.getReqClick()));
	}

	
	
	@Override
	public void onClick(Player player, DialogerNPC npc) {
		
		Action action = new TalkAction(player, this, npc);
		
		PlayerQuestHelper.sendAction(player, action);
	}
	
	@Override
	public CompleteTalkTaskDialog clone() {
		return new CompleteTalkTaskDialog(this);
	}
}