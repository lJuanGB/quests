package com.gmail.lJuanGBMinecraft.NPC;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.DialogerAPI.NPC.Dialog;
import com.gmail.lJuanGBMinecraft.DialogerAPI.NPC.DialogerNPC;
import com.gmail.lJuanGBMinecraft.DialogerAPI.Requirements.Requirement;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class StartQuestDialog extends Dialog{

	private final Quest quest;
	
	
	
	public StartQuestDialog(String trigger, String response, List<Requirement> reqShow, List<Requirement> reqClick, Quest quest) {
		super(trigger, response, reqShow, reqClick);
		
		this.quest = quest;
	}
	
	public StartQuestDialog(String trigger, String response, List<Requirement> reqShow, Quest quest) {
		this(trigger, response, reqShow, new ArrayList<Requirement>(), quest);
	}
	
	public StartQuestDialog(String trigger, String response, Quest quest) {
		this(trigger, response, new ArrayList<Requirement>(), new ArrayList<Requirement>(), quest);
	}
	
	public StartQuestDialog(StartQuestDialog dialog) {
		this(dialog.getRawTrigger(), dialog.getRawResponse(), new ArrayList<Requirement>(dialog.getReqShow()), new ArrayList<Requirement>(dialog.getReqClick()), dialog.getQuest());
	}

	
	
	public Quest getQuest() {
		return quest;
	}
	
	
	@Override
	public void onClick(Player player, DialogerNPC npc) {
		
		PlayerQuestHelper.giveQuest(player, quest, false);
	}
	
	
	@Override
	public StartQuestDialog clone() {
		return new StartQuestDialog(this);
	}
}
