package com.gmail.lJuanGBMinecraft.Requirement;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.DialogerAPI.Requirements.Requirement;
import com.gmail.lJuanGBMinecraft.Quest.NamespacedKey;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Utils.PlayerQuestHelper;

public class QuestRequirement implements Requirement{

	private final NamespacedKey key;
	
	
	
	public QuestRequirement(NamespacedKey key) {
		
		this.key = key;
	}
	
	public QuestRequirement(Quest quest) {
		this(quest.getID());
	}
	
	
	
	public NamespacedKey getKey() {
		return key;
	}

	
	
	@Override
	public boolean doesPlayerMeet(Player player) {
		
		return PlayerQuestHelper.isQuestCompleted(player, key);
	}
	
	
	
	

}
