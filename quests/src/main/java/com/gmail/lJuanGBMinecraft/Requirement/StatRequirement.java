package com.gmail.lJuanGBMinecraft.Requirement;

import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.DialogerAPI.Requirements.Requirement;

/**
 * A requirement based on MineZ stats.
 * 
 * @author lJuanGB
 *
 */
public class StatRequirement implements Requirement{


	private final String statID;
	private final int goal;
	
	
	public StatRequirement(String statID, int goal) {
		this.statID = statID;
		this.goal = goal;
	}


	public String getStatID() {
		return statID;
	}

	public int getGoal() {
		return goal;
	}
	
	
	public boolean doesPlayerMeet(Player player) {
		
		//TODO implement with MineZ code
		
		return true;
	}
}
