package com.gmail.lJuanGBMinecraft.Requirement;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gmail.lJuanGBMinecraft.DialogerAPI.Requirements.Requirement;
import com.gmail.lJuanGBMinecraft.Utils.UtilMethods;

/**
 * A requirement based on having to have certain items in the inventory.
 * Uses a soft equal check for the items. See UtilMethods.areItemsSimilar()
 * 
 * @author lJuanGB
 *
 */
public class InventoryRequirement implements Requirement{

	private final ItemStack[] itemsNeeded;

	
	public InventoryRequirement(ItemStack[] itemsNeeded) {
		this.itemsNeeded = itemsNeeded;
	}
	
	public InventoryRequirement(List<ItemStack> itemsNeeded) {
		this(itemsNeeded.toArray( new ItemStack[0] ));
	}
	
	
	public ItemStack[] getItemsNeeded() {
		return itemsNeeded;
	}


	@Override
	public boolean doesPlayerMeet(Player player) {
		
		return UtilMethods.inventoryContainsItems(player.getInventory(), itemsNeeded);
	}
	
	
}
