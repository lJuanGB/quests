package com.gmail.lJuanGBMinecraft.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.gmail.lJuanGBMinecraft.Quest.Quest;

/**
 * Event fired when a quest is completed
 * 
 * @author lJuanGB
 *
 */
public class QuestCompleteEvent extends PlayerEvent {

	
	private final Quest quest;
	
	
	public QuestCompleteEvent(Player who, Quest quest) {
		super(who);
		this.quest = quest;
	}

	
	
	public Quest getQuest() {
		return quest;
	}
	
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
