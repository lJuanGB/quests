package com.gmail.lJuanGBMinecraft.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.gmail.lJuanGBMinecraft.Task.AcceptedTask;
import com.gmail.lJuanGBMinecraft.Task.Action;

/**
 * Event fired when an action is about to count.
 * 
 * @author lJuanGB
 *
 */
public class ActionCountEvent extends PlayerEvent implements Cancellable{

	
	private final AcceptedTask task;
	private final Action action;
	
	
	public ActionCountEvent(Player who, AcceptedTask task, Action action) {
		super(who);
		
		this.task = task;
		this.action = action;
	}
	
	
	public AcceptedTask getTask() {
		return task;
	}

	public Action getAction() {
		return action;
	}
	

	private boolean cancelled = false;
	
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}



}

