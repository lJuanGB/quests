package com.gmail.lJuanGBMinecraft.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.gmail.lJuanGBMinecraft.Quest.Quest;

/**
 * Event fired when a quest is completed
 * 
 * @author lJuanGB
 *
 */
public class QuestCancelEvent extends PlayerEvent implements Cancellable{
	
	private final Quest quest;
	
	
	public QuestCancelEvent(Player who, Quest quest) {
		super(who);
		this.quest = quest;
	}

	
	
	public Quest getQuest() {
		return quest;
	}
	
	
	private boolean cancelled = false;
	
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
	
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
