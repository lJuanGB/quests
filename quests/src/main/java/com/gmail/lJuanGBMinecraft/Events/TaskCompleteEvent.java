package com.gmail.lJuanGBMinecraft.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.gmail.lJuanGBMinecraft.Task.AcceptedTask;

/**
 * Fired when a task is completed
 * 
 * @author lJuanGB
 *
 */
public class TaskCompleteEvent extends PlayerEvent {

	
	private final AcceptedTask task;
	
	
	public TaskCompleteEvent(Player who, AcceptedTask task) {
		super(who);
		this.task = task;
	}

	
	
	public AcceptedTask getTask() {
		return task;
	}
	
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}

