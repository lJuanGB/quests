package com.gmail.lJuanGBMinecraft.Utils.Region;

import org.bukkit.util.Vector;

/**
 * A 3D volume defined from two corners, creating a box.
 * 
 * @author lJuanGB
 *
 */
public class Box implements Volume{

	private final Vector lowCorner;
	private final Vector highCorner;
	
	
	public Box(Vector corner1, Vector corner2) {
		this.lowCorner = Vector.getMinimum(corner1, corner2);
		this.highCorner = Vector.getMaximum(corner1, corner2);
	}
	
	@Override
	public boolean isInside(Vector pos) {
		
		if(highCorner.getX() < pos.getX() || lowCorner.getX() > pos.getX()) return false;
		if(highCorner.getY() < pos.getY() || lowCorner.getY() > pos.getY()) return false;
		if(highCorner.getZ() < pos.getZ() || lowCorner.getZ() > pos.getZ()) return false;

		return true;
	}

	@Override
	public boolean isEquivalent(Volume vol) {
		
		if(!(vol instanceof Box)) return false;
		
		Box box2 = (Box) vol;
		
		if(!this.lowCorner.equals(box2.lowCorner)) return false;
		if(!this.highCorner.equals(box2.highCorner)) return false;

		return true;
	}
	
	
	public Vector getLowCorner() {
		return lowCorner;
	}

	public Vector getHighCorner() {
		return highCorner;
	}
	
	@Override
	public Vector getCenter() {
		return lowCorner.getMidpoint(highCorner);
	}

}
