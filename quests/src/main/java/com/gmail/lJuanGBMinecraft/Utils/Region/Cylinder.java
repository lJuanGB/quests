package com.gmail.lJuanGBMinecraft.Utils.Region;

import org.bukkit.util.Vector;

/**
 * A 3D volume defined with a circular area along the x-z plane and a height in the y direction
 * 
 * @author lJuanGB
 *
 */
public class Cylinder implements Volume{

	private final int minY;
	private final int maxY;
	
	private final int centerX;
	private final int centerZ;
	private final double squaredRadius;
	private final double radius;
	
	
	public Cylinder(int minY, int maxY, int centerX, int centerZ, double radius) {
		this.minY = minY;
		this.maxY = maxY;
		this.centerX = centerX;
		this.centerZ = centerZ;
		this.squaredRadius = Math.pow(radius, 2);
		this.radius = radius;
	}


	@Override
	public boolean isInside(Vector pos) {
		
		if(pos.getY() > maxY || pos.getY() < minY) return false;
		
		return Math.pow(pos.getX() - centerX, 2) + Math.pow(pos.getZ() - centerZ, 2) <= squaredRadius;
	}
	
	
	@Override
	public boolean isEquivalent(Volume vol) {
		
		if(!(vol instanceof Cylinder)) return false;
		
		Cylinder cyl2 = (Cylinder) vol;
		
		if(minY != cyl2.getLowY()) return false;
		if(maxY != cyl2.getHighY()) return false;
		if(centerX != cyl2.getCenterX()) return false;
		if(centerZ != cyl2.getCenterZ()) return false;
		if(radius != cyl2.getRadius()) return false;

		return true;
	}
	
	public int getLowY() {
		return minY;
	}


	public int getHighY() {
		return maxY;
	}


	public int getCenterX() {
		return centerX;
	}


	public int getCenterZ() {
		return centerZ;
	}


	public double getSquaredRadius() {
		return squaredRadius;
	}


	public double getRadius() {
		return radius;
	}
	
	@Override
	public Vector getCenter() {
		
		return new Vector(centerX, maxY - (maxY - minY)/2, centerZ);
	}
	
}
