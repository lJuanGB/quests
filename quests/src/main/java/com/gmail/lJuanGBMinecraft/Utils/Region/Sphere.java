package com.gmail.lJuanGBMinecraft.Utils.Region;

import org.bukkit.util.Vector;

/**
 * A 3D volume defined with a center and a radius.
 * 
 * @author lJuanGB
 *
 */
public class Sphere implements Volume{

	private final Vector center;
	private final double squaredRadius;
	private final double radius;
	
	public Sphere(Vector center, double radius) {
		
		this.center = center;
		this.squaredRadius = Math.pow(radius, 2);
		this.radius = radius;
	}
	
	@Override
	public boolean isInside(Vector pos) {
		return center.distanceSquared(pos) < squaredRadius;
	}

	
	@Override
	public boolean isEquivalent(Volume vol) {
		
		if(!(vol instanceof Sphere)) return false;
		
		Sphere sph2 = (Sphere) vol;
		
		if(!center.equals(sph2.getCenter())) return false;
		if(radius != sph2.getRadius()) return false;

		return true;
	}
	
	
	
	public Vector getCenter() {
		return center;
	}

	public double getSquaredRadius() {
		return squaredRadius;
	}

	public double getRadius() {
		return radius;
	}
	
}
