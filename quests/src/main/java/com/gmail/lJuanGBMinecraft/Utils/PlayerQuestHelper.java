package com.gmail.lJuanGBMinecraft.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;
import com.gmail.lJuanGBMinecraft.Events.QuestCancelEvent;
import com.gmail.lJuanGBMinecraft.Events.QuestCompleteEvent;
import com.gmail.lJuanGBMinecraft.Events.QuestStartEvent;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageMap;
import com.gmail.lJuanGBMinecraft.LanguageAPI.LanguageVariable;
import com.gmail.lJuanGBMinecraft.LanguageAPI.MessagePosition;
import com.gmail.lJuanGBMinecraft.Quest.AcceptedQuest;
import com.gmail.lJuanGBMinecraft.Quest.NamespacedKey;
import com.gmail.lJuanGBMinecraft.Quest.Quest;
import com.gmail.lJuanGBMinecraft.Task.Action;
import com.gmail.lJuanGBMinecraft.Task.LocalizedTask;
import com.gmail.lJuanGBMinecraft.Task.Task;
import com.gmail.lJuanGBMinecraft.Utils.Region.Region;

public class PlayerQuestHelper {

	public static void sendAction(Player player, Action action) {
		
		for(AcceptedQuest aQuest : getAcceptedQuests(player).toArray(new AcceptedQuest[0])) {
			aQuest.sendAction(action);
		}
	}
	
	
	public static final String acceptedQuestsKey = "acceptedQuests";
	public static final String completedQuestsKey = "completedQuests";
	
	@SuppressWarnings("unchecked")
	public static List<AcceptedQuest> getAcceptedQuests(Player player) {
		
		Object obj = UtilMethods.getMetadataValue(player, acceptedQuestsKey);
		
		if(obj == null) {
			
			List<AcceptedQuest> list = new ArrayList<AcceptedQuest>();
			
			UtilMethods.setMetadataValue(player, acceptedQuestsKey, list);
			
			return list;
			
		} else {
			
			return (List<AcceptedQuest>) obj;
		}
	}
	
	public static List<AcceptedQuest> getUnmodifiableAcceptedQuests(Player player){
		return Collections.unmodifiableList(getAcceptedQuests(player));
	}
	
	public static List<NamespacedKey> getAcceptedQuestsKeys(Player player){
		
		List<NamespacedKey> list = new ArrayList<NamespacedKey>();
		
		for(AcceptedQuest quest : getAcceptedQuests(player)) {
			list.add(quest.getQuest().getID());
		}
		
		return list;
	}
	
	public static boolean isQuestAccepted(Player player, Quest quest) {
		
		return getAcceptedQuestsKeys(player).contains(quest.getID());
	}
	
	public static void removeAcceptedQuest(Player player, Quest quest) {
		
		List<AcceptedQuest> aQuests = getAcceptedQuests(player);
		
		for(AcceptedQuest q : aQuests.toArray(new AcceptedQuest[0])) {
			
			if(q.getQuest().getID().equals(quest.getID())) aQuests.remove(q);
		}
	}
	
	public static AcceptedQuest getAcceptedQuest(Player player, Quest quest) {
		
		for(AcceptedQuest aQuest : getAcceptedQuests(player)) {
			
			if(aQuest.getQuest().equals(quest)) return aQuest;
		}
		
		return null;
	}
	
	public static void setAcceptedQuests(Player player, List<AcceptedQuest> quests) {
		
		for(AcceptedQuest q : quests.toArray(new AcceptedQuest[0])) {
			
			if(q.getQuest().isLegacy()) quests.remove(q);
		}
		
		UtilMethods.setMetadataValue(player, acceptedQuestsKey, quests);
	}
	
	
	@SuppressWarnings("unchecked")
	public static List<NamespacedKey> getCompletedQuestsKeys(Player player) {
		
		Object obj = UtilMethods.getMetadataValue(player, completedQuestsKey);
		
		if(obj == null) {
			
			List<NamespacedKey> list = new ArrayList<NamespacedKey>();
			
			UtilMethods.setMetadataValue(player, completedQuestsKey, list);
			
			return list;
			
		} else {
			
			return (List<NamespacedKey>) obj;
		}
	}
	
	public static List<NamespacedKey> getUnmodifiableCompletedQuestsKeys(Player player) {
		return Collections.unmodifiableList(getCompletedQuestsKeys(player));
	}
	
	public static boolean isQuestCompleted(Player player, Quest quest) {
		
		return getCompletedQuestsKeys(player).contains(quest.getID());
	}
	
	public static boolean isQuestCompleted(Player player, NamespacedKey key) {
		
		return getCompletedQuestsKeys(player).contains(key);
	}
	
	public static void setCompletedQuestsKeys(Player player, List<NamespacedKey> keys) {
		
		UtilMethods.setMetadataValue(player, completedQuestsKey, keys);
	}
	
	/**
	 * 
	 * @param aQuest All quests to check
	 * @param loc The regions returned will have this location inside
	 * @param classes The classes of the tasks that should be checked for regions
	 * 
	 * @return A map with the regions that contain the location as key and the quest
	 * from they come as value
	 */
	public static Map<Region, Quest> getRegions(List<AcceptedQuest> aQuest, Location loc, @SuppressWarnings("unchecked") Class<? extends LocalizedTask>... classes){
		
		Map<Region, Quest> regions = new HashMap<Region, Quest>();
				
		for(AcceptedQuest quest : aQuest) {
			
			for(Task task : quest.getQuest().getAllTasks()) {
				
				for(Class<? extends LocalizedTask> clazz : classes) {
					
					if(clazz.isInstance(task)) {
						
						LocalizedTask locTask = clazz.cast(task);
						regions.put(locTask.getRegion(), quest.getQuest());
					}
				}
			}
		}
		
				
		for(Region region : regions.keySet().toArray(new Region[0])) {
			
			if( !region.isInside(loc) ) {
				regions.remove(region);
			}
		}
		
		return regions;
	}
	
	
	public static void removeCompletedQuest(Player player, Quest quest) {
		
		List<AcceptedQuest> aQuests = getAcceptedQuests(player);
		
		for(AcceptedQuest q : aQuests.toArray(new AcceptedQuest[0])) {
			
			if(q.getQuest().getID().equals(quest.getID())) aQuests.remove(q);
		}
	}
	
	
	/**
	 * Tries to make a player accept a quest.
	 * Checks if it has been completed before, it has already been accepted
	 * and checks if quest is legacy.
	 * 
	 * @param player The player
	 * @param quest The quest
	 * @param silently Whether it should not send the player messages if they don't
	 * meet the requirements or they accept the quest
	 * 
	 * @return true if the quest was finally accepted, false otherwise
	 */
	public static boolean giveQuest(Player player, Quest quest, boolean silently) {

		if(!quest.canPlayerAccept(player)) {
			
			return false;
		}
		
		return acceptQuest(player, quest, silently);
	}
	
	
	public static final String pathAccepted = "announcements.quest.accepted.";
	public static final String pathCancelled = "announcements.quest.cancelled.";
	public static final String pathQCompleted = "announcements.quest.completed.";
	public static final String pathTCompleted = "announcements.task.completed.";

	public static final String title = "title";
	public static final String subtitle = "subtitle";
	public static final String chat = "chat";

	public static final String questNameKey = "questName";
	
	/**
	 * Forces a player to accept the quest
	 * Doesn't check anything. May only return false if the QuestStartEvent is cancelled.
	 * 
	 * Safer to use giveQuest().
	 * 
	 * @param player The player
	 * @param quest The quest
	 * @param silently Whether it should not send the player messages if they don't
	 * meet the requirements or they accept the quest
	 * @return false if the quest was not accepted.
	 */
	public static boolean acceptQuest(Player player, Quest quest, boolean silently) {
		
		QuestStartEvent event = new QuestStartEvent(player, quest);
		Bukkit.getPluginManager().callEvent(event);
		
		
		if(event.isCancelled()) return false;
		
		if(!silently) {
			
			List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
			String questName = LanguageMap.getMessage(quest.getPlugin(), player, quest.getNamePath());
			variables.add(new LanguageVariable(questNameKey, questName));
			
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.TITLE, 
					player, pathAccepted + title, variables);
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.SUBTITLE, 
					player, pathAccepted + subtitle, variables);
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.CHAT, 
					player, pathAccepted + chat, variables);

		}		
		
		quest.accept(player);
		
		AcceptedQuest aQuest = new AcceptedQuest(quest);
		getAcceptedQuests(player).add(aQuest);
		return true;
	}
	
	/**
	 * Forces the player to abandon the quest.
	 * Won't do anything if the quest has not been accepted.
	 * 
	 * @param player The player
	 * @param quest The quest
	 * @param silently Whether it should not send the player messages
	 * @return false if the quest was not previously accepted or the QuestCancelEvent was cancelled.
	 */
	public static boolean cancelQuest(Player player, Quest quest, boolean silently) {
		
		if(!isQuestAccepted(player, quest)) return false;
		
		
		QuestCancelEvent event = new QuestCancelEvent(player, quest);
		Bukkit.getPluginManager().callEvent(event);
		
		
		if(event.isCancelled()) return false;
		
		if(!silently) {
			
			List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
			String questName = LanguageMap.getMessage(quest.getPlugin(), player, quest.getNamePath());
			variables.add(new LanguageVariable(questNameKey, questName));
			
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.TITLE, 
					player, pathCancelled + title, variables);
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.SUBTITLE, 
					player, pathCancelled + subtitle, variables);
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.CHAT, 
					player, pathCancelled + chat, variables);

		}
		
		removeAcceptedQuest(player, quest);
		return true;
	}
	
	/**
	 * Artificially completes the quest for the player.
	 * Won't do anything if the quest has not been accepted.
	 * 
	 * @param player The player
	 * @param quest The quest
	 * @param silently Whether it should not send the player messages
	 * @return false if the quest was not previously accepted.
	 */
	public static boolean completeQuest(Player player, Quest quest, boolean silently) {
		
		if(!isQuestAccepted(player, quest)) return false;		
		
		QuestCompleteEvent event = new QuestCompleteEvent(player, quest);
		Bukkit.getPluginManager().callEvent(event);
		
		
		if(!silently) {
			
			List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
			String questName = LanguageMap.getMessage(quest.getPlugin(), player, quest.getNamePath());
			variables.add(new LanguageVariable(questNameKey, questName));
			
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.TITLE, 
					player, pathQCompleted + title, variables);
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.SUBTITLE, 
					player, pathQCompleted + subtitle, variables);
			LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.CHAT, 
					player, pathQCompleted + chat, variables);

		}
		
		quest.rewardPlayer(player);
		removeAcceptedQuest(player, quest);
		getCompletedQuestsKeys(player).add(quest.getID());
		return true;
	}
	
	
	public static void completeTaskMessage(Player player, Quest quest) {
		
		List<LanguageVariable> variables = new ArrayList<LanguageVariable>();
		String questName = LanguageMap.getMessage(quest.getPlugin(), player, quest.getNamePath());
		variables.add(new LanguageVariable(questNameKey, questName));
		
		LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.TITLE, 
				player, pathTCompleted + title, variables);
		LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.SUBTITLE, 
				player, pathTCompleted + subtitle, variables);
		LanguageMap.sendMessage(QuestsAPIPlugin.getInstance(), MessagePosition.CHAT, 
				player, pathTCompleted + chat, variables);

	}
	
	
}
