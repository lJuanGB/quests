package com.gmail.lJuanGBMinecraft.Utils.Region;

import org.bukkit.Location;
import org.bukkit.util.Vector;

/**
 * A region is defined spaced in a world of minecraft with its own name.
 * 
 * @author lJuanGB
 *
 */
public class Region {

	private final Volume volume;
	private final String regionName;
	
	private final String world;
	
	/**
	 * 
	 * @param volume The 3D volume that defines it
	 * @param regionName The display name of this region
	 * @param world The world where this region is located
	 */
	public Region(Volume volume, String regionName, String world) {
		super();
		this.volume = volume;
		this.regionName = regionName;
		this.world = world;
	}


	public Volume getVolume() {
		return volume;
	}

	public String getRegionName() {
		return regionName;
	}
	
	public String getWorld() {
		return world;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null) return false;

		if(!(obj instanceof Region)) return false;
				
		Region reg = (Region) obj;
		
		if(!reg.getRegionName().equals(regionName)) return false;
		if(!reg.getVolume().isEquivalent(volume)) return false;
		
		return true;
	}
	
	public boolean isInside(Vector pos, String world) {
		
		if(world != getWorld()) return false;
		
		return volume.isInside(pos);
	}
	
	public boolean isInside(Location loc) {
		return isInside(loc.toVector(), loc.getWorld().getName());
	}
	
}
