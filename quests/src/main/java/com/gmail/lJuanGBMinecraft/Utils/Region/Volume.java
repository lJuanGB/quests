package com.gmail.lJuanGBMinecraft.Utils.Region;

import org.bukkit.util.Vector;

/**
 * A 3D construction.
 * 
 * @author lJuanGB
 *
 */
public interface Volume {

	/**
	 * 
	 * @param vector
	 * @return true if the vector coordinates are inside the volume
	 */
	public boolean isInside(Vector vector);
	
	/**
	 * 
	 * @param vol 
	 * @return true if vol equals the volume
	 */
	public boolean isEquivalent(Volume vol);
	
	/**
	 * 
	 * @return The centroid of the 3D region
	 */
	public Vector getCenter();
}
