package com.gmail.lJuanGBMinecraft.Utils;

import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ProxiedCommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.minecart.CommandMinecart;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;

import com.gmail.lJuanGBMinecraft.QuestsAPIPlugin;

public class UtilMethods {

	public static String formatSecondsToClock(int totalSeconds) {
		
		int hours = totalSeconds / 3600;
		int minutes = (totalSeconds % 3600) / 60;
		int seconds = totalSeconds % 60;

		if(hours > 0) return String.format("%02d:%02d:%02d", hours, minutes, seconds);
		
		if(minutes > 0)  return String.format("%02d:%02d", minutes, seconds);
		
		return String.format("%02d", minutes, seconds);
	}
	
	
	public static final String path = "miscellaneous.clock.";
	public static final String hours = "hours";
	public static final String minutes = "minutes";
	public static final String seconds = "seconds";
	public static final String hour = "hour";
	public static final String minute = "minute";
	public static final String second = "second";
	
	public static String divideSeconds(int totalSeconds, String locale) {
		
		int hours = totalSeconds / 3600;
		int minutes = (totalSeconds % 3600) / 60;
		int seconds = totalSeconds % 60;

		String result = "";
		
		if(hours > 1) {
			
			String hoursString = QuestsAPIPlugin.api.getMessage(locale, path + UtilMethods.hours);
			result += hours + " " + hoursString;
			
		}else if(hours == 1) {
			
			String hourString = QuestsAPIPlugin.api.getMessage(locale, path + hour);
			result += hours + " " + hourString;
		}

		if(result.length() == 0) result += " ";
		
		if(minutes > 1) {
			
			String minutesString = QuestsAPIPlugin.api.getMessage(locale, path + UtilMethods.minutes);
			result += minutes + " " + minutesString;
			
		}else if(minutes == 1) {
			
			String minuteString = QuestsAPIPlugin.api.getMessage(locale, path + minute);
			result += minutes + " " + minuteString;
		}
		
		if(result.length() == 0) result += " ";
		
		if(seconds > 1) {
			
			String secondsString = QuestsAPIPlugin.api.getMessage(locale, path + UtilMethods.seconds);
			result += seconds + " " + secondsString;
			
		}else if(minutes == 1) {
			
			String secondString = QuestsAPIPlugin.api.getMessage(locale, path + UtilMethods.second);
			result += seconds + " " + secondString;
		}

		return result;
	}
	
	
	
	
	public static String itemToString(ItemStack item) {
		
		String itemName = item.hasItemMeta() && item.getItemMeta().hasDisplayName() ? item.getItemMeta().getDisplayName() : null;
		
		if(itemName == null) {
			
			return WordUtils.capitalizeFully(item.getType().toString().replaceAll("_", " "));
			
		} else {
			
			return itemName;
		}
	}
	
	public static boolean areItemsSimilar(ItemStack item1, ItemStack item2) {
		
		if(item1 == null && item2 == null) return true;
		if(item1 == null || item2 == null) return false;
		
		String item1Name = item1.hasItemMeta() && item1.getItemMeta().hasDisplayName() ? item1.getItemMeta().getDisplayName() : null;
		String item2Name = item2.hasItemMeta() && item2.getItemMeta().hasDisplayName() ? item2.getItemMeta().getDisplayName() : null;
		
		if(item1Name == null && item2Name == null) {
			return item1.getType().equals(item2.getType());
		}
		
		if(item1Name != null && item2Name != null) {
			return item1Name.equals(item2Name);
		}
		
		return false;
	}
	
	public static boolean inventoryContainsItems(Inventory inv, ItemStack... items) {
		
		int[] itemsToBeSeen = new int[items.length];
		
		for(int i = 0; i < items.length; i++) {
			
			ItemStack item = items[i];
			itemsToBeSeen[i] = item.getAmount();
			
			for(ItemStack itemInv : inv) {
				
				if(areItemsSimilar(itemInv, item)) {
					itemsToBeSeen[i] -= itemInv.getAmount();
				}
				
				if(itemsToBeSeen[i] <= 0) break;
			}
			
			if(itemsToBeSeen[i] > 0) return false;
		}
		
		return true;
	}
	
	public static boolean removeItemsFromInventory(Inventory inv, ItemStack... items) {
		
		if(!inventoryContainsItems(inv, items)) return false;
			
		int[] itemsToBeRemoved = new int[items.length];
		
		for(int i = 0; i < items.length; i++) {
			
			ItemStack item = items[i];
			itemsToBeRemoved[i] = item.getAmount();
			
			for(ItemStack itemInv : inv) {
				
				if(areItemsSimilar(itemInv, item)) {
					
					if(itemsToBeRemoved[i] >= itemInv.getAmount()) {
						
						itemsToBeRemoved[i] -= itemInv.getAmount();
						itemInv.setAmount(0);
						
					} else {
						
						itemInv.setAmount(itemInv.getAmount() - itemsToBeRemoved[i]);
						itemsToBeRemoved[i] = 0;
						break;
					}
					
				}
				
				
			}
		}
		
		return true;
	}
	
	
	public static Object getMetadataValue(Metadatable metadatable, String path) {
		
		for(MetadataValue value : metadatable.getMetadata(path)) {
			
			if(value.getOwningPlugin().equals(QuestsAPIPlugin.getInstance())) return value.value();
		}
			
		return null;
	}
	
	public static void setMetadataValue(Metadatable metadatable, String path, Object obj) {
		
		metadatable.setMetadata(path, new FixedMetadataValue(QuestsAPIPlugin.getInstance(), obj));
	}
	
	public static boolean hasMetadataValue(Metadatable metadatable, String path) {
		
		for(MetadataValue value : metadatable.getMetadata(path)) {
			
			if(value.getOwningPlugin().equals(QuestsAPIPlugin.getInstance())) return true;
		}
			
		return false;
	}
	
	
	
	public static <E> E getRandom(Collection<E> collection) {
		
		if(collection.isEmpty()) return null;
		
		int size = collection.size();
		
		Random rand = new Random();
		int randomInt = rand.nextInt( size );
		
		int i = 0;
		for(E e : collection) {
			
			if(randomInt == i) return e;
			
			i++;
		}
		
		return null;
	}
	
	
	
	public static boolean hasLocation(CommandSender sender) {
		
		if(sender instanceof Player) return true;
		if(sender instanceof BlockCommandSender) return true;
		if(sender instanceof ProxiedCommandSender) {
			
			if(((ProxiedCommandSender) sender).getCallee() instanceof Entity) return true;
		}
		if(sender instanceof CommandMinecart) return true;
		
		return false;
	}
	
	public static Location getLocation(CommandSender sender) {
		
		if(!hasLocation(sender)) return null;
		
		if(sender instanceof Player) {
				
			return ((Player) sender).getLocation();
		} 
		
		if (sender instanceof ProxiedCommandSender) {
			
			ProxiedCommandSender proxSender = (ProxiedCommandSender) sender;
		
			if(proxSender.getCallee() instanceof Entity) {
				
				return ((Entity) proxSender.getCallee()).getLocation();
				
			} else {
				
				return null;
			}
		}
		
		if (sender instanceof BlockCommandSender) {
			
			return ((BlockCommandSender) sender).getBlock().getLocation();
		}
		
		if(sender instanceof CommandMinecart) {
			
			return ((CommandMinecart) sender).getLocation();
		}


		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public static <T extends Entity> T getClosestEntity(Location loc, Class<T> type, Entity... avoid) {
		
		List<Entity> entities = loc.getWorld().getEntities();
		
		double minDistance = Double.MAX_VALUE;
		T minEntity = null;
		
		for(Entity ent : entities) {
			
			if(ArrayUtils.contains(avoid, ent)) continue;
			if(!type.isInstance( ent )) continue;
			
			double distance = ent.getLocation().distanceSquared(loc);
			
			if(distance < minDistance) {
				minDistance = distance;
				minEntity = (T) ent;
			}
		}
		
		return (T) minEntity;
	}
	
	
	
	public static String[] splitStringIntoLines(String string, int lineLength) {
		
		String wrapped = WordUtils.wrap(string, lineLength);
		
		return wrapped.split(System.lineSeparator());
	}
}
